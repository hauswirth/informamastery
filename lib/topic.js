Meteor.methods({
	addTopic: function(themeId) {
		console.log("Meteor.methods.addTopic("+themeId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit a course.");
		}
		var theme = Themes.findOne({_id: themeId});
		if (!theme) {
			throw new Meteor.Error("theme-does-not-exist", "The specified theme does not exist.");
		}
		if (!isInstructor(this.userId, theme.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to edit this course.");
		}
		var now = new Date();
		return Topics.insert({
			courseId: theme.courseId,
			themeId: themeId,
			log: [{what: "Created", when: now, who: this.userId}],
		});
	},
	updateTopic: function(topicId, title, description, why, studyGuide, themeId, masteryRequiredForGrade) {
		console.log("Meteor.methods.updateTopic("+topicId+", "+title+", "+description+", "+why+","+studyGuide+", "+themeId+", "+masteryRequiredForGrade+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit a topic.");
		}
		var topic = Topics.findOne({_id: topicId});
		if (!topic) {
			throw new Meteor.Error("topic-does-not-exist", "The specified topic does not exist.");
		}
		if (!isInstructor(this.userId, topic.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to edit this topic.");
		}
		var theme = Themes.findOne({_id: themeId});
		if (!theme) {
			throw new Meteor.Error("theme-does-not-exist", "The specified theme does not exist.");
		}
		var now = new Date();
		return Topics.update({_id: topicId}, {
			$set: {
				title: title,
				description: description,
				why: why,
				studyGuide: studyGuide,
				themeId: themeId,
				masteryRequiredForGrade: masteryRequiredForGrade,
			},
			$push: {
				log: {what: "Updated", when: now, who: this.userId},
			},
		});
	},
	showTopic: function(topicId) {
		console.log("Meteor.methods.showTopic("+topicId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to show a topic.");
		}
		var topic = Topics.findOne({_id: topicId});
		if (!topic) {
			throw new Meteor.Error("topic-does-not-exist", "The specified topic does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, topic.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to show this topic.");
		}
		var now = new Date();
		return Topics.update({_id: topicId}, {
			$set: {
				status: "showing",
			},
			$push: {
				log: {what: "Shown", when: now, who: this.userId},
			},
		});
	},
	hideTopic: function(topicId) {
		console.log("Meteor.methods.hideTopic("+topicId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to hide a topic.");
		}
		var topic = Topics.findOne({_id: topicId});
		if (!topic) {
			throw new Meteor.Error("topic-does-not-exist", "The specified topic does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, topic.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to hide this topic.");
		}
		var now = new Date();
		return Topics.update({_id: topicId}, {
			$set: {
				status: "hidden",
			},
			$push: {
				log: {what: "Hidden", when: now, who: this.userId},
			},
		});
	},
	removeTopic: function(topicId) {
		console.log("Meteor.methods.removeTopic("+topicId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit a course.");
		}
		var topic = Topics.findOne({_id: topicId});
		if (!topic) {
			throw new Meteor.Error("topic-does-not-exist", "The specified topic does not exist.");
		}
		if (!isInstructor(this.userId, topic.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to edit this topic.");
		}
		var now = new Date();
		// don't delete, but mark as removed
		return Topics.update({_id: topicId}, {
			$set: {
				removed: true,
			},
			$push: {
				log: {what: "Removed", when: now, who: this.userId},
			},
		});
	},
});
