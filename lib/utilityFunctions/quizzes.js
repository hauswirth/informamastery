getPastQuizzes = function(courseId) {
  console.log("getPastQuizzes("+courseId+")");
  // quizzes which have a stopDate that's earlier than now
  var now = new Date();
  return Quizzes.find({courseId: courseId, removed: false, stopDate: {$lt: now}}, {sort: {date: 1}});
};

getPastNonHiddenQuizzes = function(courseId) {
  console.log("getPastNonHiddenQuizzes("+courseId+")");
  // quizzes which have a stopDate that's earlier than now
  var now = new Date();
  return Quizzes.find({courseId: courseId, removed: false, status: {$ne: "hidden"}, stopDate: {$lt: now}}, {sort: {date: 1}});
};


getQuizAttempt = function(quizId, userId) {
  console.log("getQuizAttempt("+quizId+", "+userId+")")
  // the LAST attempt of that quiz!
  var quiz = Quizzes.findOne({_id: quizId});
  var attempt = QuizAttempts.findOne({quizId: quizId, studentId: userId, quizStartDate: quiz.startDate});
  console.log(attempt);
  return attempt;
};

isQuizAttemptItemCorrect = function(quizAttempt, practiceProblem) {
  console.log("isQuizAttemptItemCorrect(", quizAttempt, ", ", practiceProblem, ")");
  if (quizAttempt && quizAttempt.solutions) {
    var attemptedSolutions = quizAttempt.solutions;
    var attemptedSolution = _.find(attemptedSolutions, function(solution) {return solution.practiceProblemId==practiceProblem._id});
    var attemptedChoices = attemptedSolution.choices;
    console.log("  attemptedChoices:", attemptedChoices);
    var correctChoices = practiceProblem.choices;
    console.log("  correctChoices:", correctChoices);
    var containsMistake = false;
    _.forEach(correctChoices, function(correctChoice) {if (correctChoice.text && attemptedChoices[correctChoice.label]!=correctChoice.isCorrect) {containsMistake = true;}});
    console.log("  containsMistake:", containsMistake);
    return !containsMistake;
  } else {
    return false;
  }
}

countCorrectQuizAttemptItems = function(quizAttempt) {
  console.log("countCorrectQuizAttemptItems(", quizAttempt ,")");
  if (quizAttempt) {
    var quiz = Quizzes.findOne({_id: quizAttempt.quizId});
    var practiceProblems = PracticeProblems.find({_id: {$in: quiz.practiceProblemIds}});
    var count = 0;
    practiceProblems.forEach(function(practiceProblem) {
    if (isQuizAttemptItemCorrect(quizAttempt, practiceProblem)) {
      count++;
    }
  });
    return count;
  } else {
    return 0;
  }
}

computeMaxOverallQuizScore = function(quizId) {
  console.log("computeMaxOverallQuizScore("+quizId+")");
  var quiz = Quizzes.findOne({_id: quizId});
  var practiceProblems = PracticeProblems.find({_id: {$in: quiz.practiceProblemIds}});
  var count = 0;
  practiceProblems.forEach(function(practiceProblem) {
    count++;
  });
  console.log(count);
  return count;
};

computeOverallQuizScore = function(quizAttempt) {
  console.log("computeOverallQuizScore(", quizAttempt, ")");
  return countCorrectQuizAttemptItems(quizAttempt);
};
