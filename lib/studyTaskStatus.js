Meteor.methods({
	startStudyTask: function(studyTaskId) {
		var now = new Date();
		logMethodCall(now, "startStudyTask", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update the study task status.");
		}
		var studyTask = StudyTasks.findOne({_id: studyTaskId});
		if (!studyTask) {
			throw new Meteor.Error("unknown-study-task", "There is no study task with the given id");
		}
		// we log this on purpose, so if the database breaks down, we still have all submissions in the log
		console.log("Meteor.methods.startStudyTask: studyTaskId:", studyTaskId, "userId:", this.userId, "date:", now);
		StudyTaskStatus.update({
			userId: this.userId,
			courseId: studyTask.courseId,
			themeId: studyTask.themeId,
			topicId: studyTask.topicId,
			studyTaskId: studyTaskId,
		}, {
			$set: {
				started: true,
				startedDate: now,
			},
			$push: {
				log: {what: "Started", when: now, who: this.userId},
			},
		}, {
			upsert: true,
		});
		var studyTaskStatusId = StudyTaskStatus.findOne({
			userId: this.userId,
			courseId: studyTask.courseId,
			studyTaskId: studyTaskId,
		})._id;
		FeedEvents.insert({
			userId: this.userId,
			courseId: studyTask.courseId,
			date: now,
			active: true,
			event: "IStartedStudyTask",
			studyTaskStatusId: studyTaskStatusId,
			studyTaskId: studyTask._id,
		});
		AnalyticsLog.insert({
			userId: this.userId,
			courseId: studyTask.courseId,
			serverDate: now,
			event: "startStudyTask",
			contents: {
				studyTaskId: studyTask._id,
				studyTaskStatusId: studyTaskStatusId,
			},
		});
		return studyTaskStatusId;
	},
	finishStudyTask: function(studyTaskId) {
		var now = new Date();
		logMethodCall(now, "finishStudyTask", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update the study task status.");
		}
		var studyTask = StudyTasks.findOne({_id: studyTaskId});
		if (!studyTask) {
			throw new Meteor.Error("unknown-study-task", "There is no study task with the given id");
		}
		// we log this on purpose, so if the database breaks down, we still have all submissions in the log
		console.log("Meteor.methods.finishStudyTask: studyTaskId:", studyTaskId, "userId:", this.userId, "date:", now);
		StudyTaskStatus.update({
			userId: this.userId,
			courseId: studyTask.courseId,
			themeId: studyTask.themeId,
			topicId: studyTask.topicId,
			studyTaskId: studyTaskId,
		}, {
			$set: {
				finished: true,
				finishedDate: now,
			},
			$push: {
				log: {what: "Finished", when: now, who: this.userId},
			},
		}, {
			upsert: true,
		});
		var studyTaskStatusId = StudyTaskStatus.findOne({
			userId: this.userId,
			courseId: studyTask.courseId,
			studyTaskId: studyTaskId,
		})._id;
		FeedEvents.insert({
			userId: this.userId,
			courseId: studyTask.courseId,
			date: now,
			active: true,
			event: "IFinishedStudyTask",
			studyTaskId: studyTaskStatusId,
			studyTaskId: studyTask._id,
		});
		AnalyticsLog.insert({
			userId: this.userId,
			courseId: studyTask.courseId,
			serverDate: now,
			event: "finishStudyTask",
			contents: {
				studyTaskId: studyTask._id,
				studyTaskStatusId: studyTaskStatusId,
			},
		});
		return studyTaskStatusId;
	},
	updateStudyTaskRecallText: function(studyTaskId, recallText) {
		var now = new Date();
		logMethodCall(now, "updateStudyTaskRecallText", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update the study task status.");
		}
		var studyTask = StudyTasks.findOne({_id: studyTaskId});
		if (!studyTask) {
			throw new Meteor.Error("unknown-study-task", "There is no study task with the given id");
		}
		// we log this on purpose, so if the database breaks down, we still have all submissions in the log
		console.log("Meteor.methods.updateStudyTaskRecallText: studyTaskId:", studyTaskId, "userId:", this.userId, "date:", now, "recallText:", recallText);
		StudyTaskStatus.update({
			userId: this.userId,
			courseId: studyTask.courseId,
			themeId: studyTask.themeId,
			topicId: studyTask.topicId,
			studyTaskId: studyTaskId,
		}, {
			$set: {
				recallText: recallText,
				modificationDate: now,
			},
			$push: {
				log: {what: "Saved", when: now, who: this.userId, recallText: recallText},
			},
		}, {
			upsert: true,
		});
		var studyTaskStatusId = StudyTaskStatus.findOne({
			userId: this.userId,
			courseId: studyTask.courseId,
			studyTaskId: studyTaskId,
		})._id;
		FeedEvents.insert({
			userId: this.userId,
			courseId: studyTask.courseId,
			date: now,
			active: true,
			event: "IRecalledStudyTask",
			studyTaskStatusId: studyTaskStatusId,
			studyTaskId: studyTask._id,
			recallText: recallText,
		});
		AnalyticsLog.insert({
			userId: this.userId,
			courseId: studyTask.courseId,
			serverDate: now,
			event: "updateStudyTaskRecallText",
			contents: {
				studyTaskId: studyTask._id,
				studyTaskStatusId: studyTaskStatusId,
				recallText: recallText,
			},
		});
		return studyTaskStatusId;
	},
});
