Meteor.methods({
	addQuiz: function(courseId) {
		console.log("Meteor.methods.addQuiz("+courseId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to add a quiz.");
		}
		var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
		if (!isInstructor(this.userId, course._id) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to add a quiz.");
		}
		var now = new Date();
		return Quizzes.insert({
			courseId: courseId,
			creationDate: now,
			creatorId: this.userId,
			removed: false,
		});
	},
	updateQuiz: function(quizId, title, description, date, duration) {
		console.log("Meteor.methods.updateQuiz("+quizId+", "+title+", "+description+", "+date+", "+duration+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a quiz.");
		}
		var quiz = Quizzes.findOne({_id: quizId});
		if (!quiz) {
			throw new Meteor.Error("quiz-does-not-exist", "The specified quiz does not exist.");
		}
		if (!isInstructor(this.userId, quiz.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this quiz.");
		}
		var now = new Date();
		return Quizzes.update({_id: quizId}, {
			$set: {
				title: title,
				description: description,
        date: date,
        duration: duration,
			},
		});
	},
	addQuizTopic: function(quizId, topicId) {
		console.log("Meteor.methods.addQuizTopic("+quizId+", "+topicId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a quiz.");
		}
		var quiz = Quizzes.findOne({_id: quizId});
		if (!quiz) {
			throw new Meteor.Error("quiz-does-not-exist", "The specified quiz does not exist.");
		}
		if (!isInstructor(this.userId, quiz.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this quiz.");
		}
		var now = new Date();
		return Quizzes.update({_id: quizId}, {
			$addToSet: {
				topicIds: topicId,
			},
		});
	},
	removeQuizTopic: function(quizId, topicId) {
		console.log("Meteor.methods.removeQuizTopic("+quizId+", "+topicId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a quiz.");
		}
		var quiz = Quizzes.findOne({_id: quizId});
		if (!quiz) {
			throw new Meteor.Error("quiz-does-not-exist", "The specified quiz does not exist.");
		}
		if (!isInstructor(this.userId, quiz.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this quiz.");
		}
		var now = new Date();
		return Quizzes.update({_id: quizId}, {
			$pullAll: {
				topicIds: [topicId],
			},
		});
	},
  addQuizPracticeProblem: function(quizId, practiceProblemId) {
		console.log("Meteor.methods.addQuizPracticeProblem("+quizId+", "+practiceProblemId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a quiz.");
		}
		var quiz = Quizzes.findOne({_id: quizId});
		if (!quiz) {
			throw new Meteor.Error("quiz-does-not-exist", "The specified quiz does not exist.");
		}
		if (!isInstructor(this.userId, quiz.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this quiz.");
		}
		var now = new Date();
		return Quizzes.update({_id: quizId}, {
			$addToSet: {
				practiceProblemIds: practiceProblemId,
			},
		});
	},
	removeQuizPracticeProblem: function(quizId, practiceProblemId) {
		console.log("Meteor.methods.removeQuizPracticeProblem("+quizId+", "+practiceProblemId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a quiz.");
		}
		var quiz = Quizzes.findOne({_id: quizId});
		if (!quiz) {
			throw new Meteor.Error("quiz-does-not-exist", "The specified quiz does not exist.");
		}
		if (!isInstructor(this.userId, quiz.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this quiz.");
		}
		var now = new Date();
		return Quizzes.update({_id: quizId}, {
			$pullAll: {
				practiceProblemIds: [practiceProblemId],
			},
		});
	},
	showQuiz: function(quizId) {
		console.log("Meteor.methods.showQuiz("+quizId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to show a quiz.");
		}
		var quiz = Quizzes.findOne({_id: quizId});
		if (!quiz) {
			throw new Meteor.Error("quiz-does-not-exist", "The specified quiz does not exist.");
		}
		if (!isInstructor(this.userId, quiz.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to show this quiz.");
		}
		var now = new Date();
		Quizzes.update({_id: quizId}, {
			$set: {
				status: "showing",
			},
			$push: {
				log: {what: "Shown", when: now, who: this.userId},
			},
		});
	},
	hideQuiz: function(quizId) {
		console.log("Meteor.methods.hideQuiz("+quizId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to hide a quiz.");
		}
		var quiz = Quizzes.findOne({_id: quizId});
		if (!quiz) {
			throw new Meteor.Error("quiz-does-not-exist", "The specified quiz does not exist.");
		}
		if (!isInstructor(this.userId, quiz.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to hide this quiz.");
		}
		var now = new Date();
		Quizzes.update({_id: quizId}, {
			$set: {
				status: "hidden",
			},
			$push: {
				log: {what: "Hidden", when: now, who: this.userId},
			},
		});
	},
	removeQuiz: function(quizId) {
		console.log("Meteor.methods.removeQuiz("+quizId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to remove a quiz.");
		}
		var quiz = Quizzes.findOne({_id: quizId});
		if (!quiz) {
			throw new Meteor.Error("quiz-does-not-exist", "The specified quiz does not exist.");
		}
		if (!isInstructor(this.userId, quiz.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to remove this quiz.");
		}
		var now = new Date();
		//TODO: if there is extra data in other tables for this quiz, "remove" that extra data
		Quizzes.update({_id: quizId}, {
			$set: {
				removed: true,
			},
		});
	},

	startQuiz: function(quizId) {
		console.log("Meteor.methods.startQuiz("+quizId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to start a quiz.");
		}
		var quiz = Quizzes.findOne({_id: quizId});
		if (!quiz) {
			throw new Meteor.Error("quiz-does-not-exist", "The specified quiz does not exist.");
		}
		if (quiz.ongoing) {
			throw new Meteor.Error("quiz-ongoing", "The specified quiz already is ongoing.");
		}
		if (!isOnTeachingTeam(this.userId, quiz.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("not-on-teaching-team", "You must be on the teaching team of this course to start a quiz.");
		}
		var now = new Date();
		Quizzes.update({_id: quizId}, {
			$set: {
				ongoing: true,
				startDate: now,
				startedBy: this.userId,
			},
		});
	},
	stopQuiz: function(quizId) {
		console.log("Meteor.methods.stopQuiz("+quizId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to stop a quiz.");
		}
		var quiz = Quizzes.findOne({_id: quizId});
		if (!quiz) {
			throw new Meteor.Error("quiz-does-not-exist", "The specified quiz does not exist.");
		}
		if (!quiz.ongoing) {
			throw new Meteor.Error("quiz-not-ongoing", "The specified quiz is not ongoing.");
		}
		if (!isOnTeachingTeam(this.userId, quiz.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("not-on-teaching-team", "You must be on the teaching team of this course to stop a quiz.");
		}
		var now = new Date();
		Quizzes.update({_id: quizId}, {
			$set: {
				ongoing: false,
				stopDate: now,
				stoppedBy: this.userId,
			},
		});
		QuizAttempts.update({quizId: quizId}, {
			$set: {
				endDate: now,
        fresh: false,
        ongoing: false,
        finished: true,
			},
		}, {multi:true});
	},

});
