function setSkillStatus(now, callerUserId, userId, skillId, status, ready) {
	if (!callerUserId) {
		throw new Meteor.Error("logged-out", "You must be logged in");
	}
	var skill = Skills.findOne({_id: skillId});
	if (!skill) {
		throw new Meteor.Error("unknown-skill", "There is no skill with the given id");
	}
	if (!isRegisteredAsAnything(callerUserId, skill.courseId)) {
		throw new Meteor.Error("no-permission", "You must be registered");
	}
	if (!isRegisteredAsAnything(userId, skill.courseId)) {
		throw new Meteor.Error("no-permission", "The user must be registered");
	}
	if (userId!=callerUserId &&
		(!isInstructor(callerUserId, skill._id) && !isAdmin(callerUserId))) {
		throw new Meteor.Error("no-permission", "You don't have permission");
	}
	SkillStatus.update({
		studentId: userId,
		courseId: skill.courseId,
		topicId: skill.topicId,
		skillId: skillId,
	}, {
		$set: {
			ready: ready,
			status: status,
		},
		$push: { log: {
			event: "Set"+status,
			date: now,
			who: callerUserId,
		}},
	}, {
		upsert: true,
	});
	var skillStatusId = SkillStatus.findOne({
		studentId: userId,
		courseId: skill.courseId,
		topicId: skill.topicId,
		skillId: skillId,
	})._id;
	FeedEvents.insert({
		userId: userId,
		courseId: skill.courseId,
		date: now,
		active: true,
		event: "ISetSkillStatus"+status,
		skillId: skillId,
		skillStatusId: skillStatusId,
	});
	AnalyticsLog.insert({
		callerUserId: callerUserId,
		userId: userId,
		courseId: skill.courseId,
		serverDate: now,
		event: "setSkillStatus"+status,
		contents: {
			topicId: skill.topicId,
			skillId: skillId,
			skillStatusId: skillStatusId,
		},
	});
	return skillStatusId;
}

Meteor.methods({
	setUserSkillStatusCleared: function(userId, skillId) {
		var now = new Date();
		logMethodCall(now, "setUserSkillStatusCleared", arguments);
		return setSkillStatus(now, this.userId, userId, skillId, "Cleared", false);
	},
	setUserSkillStatusRed: function(userId, skillId) {
		var now = new Date();
		logMethodCall(now, "setUserSkillStatusRed", arguments);
		return setSkillStatus(now, this.userId, userId, skillId, "Red", false);
	},
	setUserSkillStatusAmber: function(userId, skillId) {
		var now = new Date();
		logMethodCall(now, "setUserSkillStatusAmber", arguments);
		return setSkillStatus(now, this.userId, userId, skillId, "Amber", false);
	},
	setUserSkillStatusGreen: function(userId, skillId) {
		var now = new Date();
		logMethodCall(now, "setUserSkillStatusGreen", arguments);
		return setSkillStatus(now, this.userId, userId, skillId, "Green", true);
	},

	setSkillStatusCleared: function(skillId) {
		var now = new Date();
		logMethodCall(now, "setSkillStatusCleared", arguments);
		return setSkillStatus(now, this.userId, this.userId, skillId, "Cleared", false);
	},
	setSkillStatusRed: function(skillId) {
		var now = new Date();
		logMethodCall(now, "setSkillStatusRed", arguments);
		return setSkillStatus(now, this.userId, this.userId, skillId, "Red", false);
	},
	setSkillStatusAmber: function(skillId) {
		var now = new Date();
		logMethodCall(now, "setSkillStatusAmber", arguments);
		return setSkillStatus(now, this.userId, this.userId, skillId, "Amber", false);
	},
	setSkillStatusGreen: function(skillId) {
		var now = new Date();
		logMethodCall(now, "setSkillStatusGreen", arguments);
		return setSkillStatus(now, this.userId, this.userId, skillId, "Green", true);
	},

});
