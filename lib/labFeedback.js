Meteor.methods({
  setLabFeedback: function(labId, key, value) {
		var now = new Date();
		logMethodCall(now, "setLabFeedback", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
    var lab = Labs.findOne({_id: labId});
		if (!lab) {
			throw new Meteor.Error("no-lab", "There is no lab with the given id.");
		}
		if (!isUserApprovedNotWithdrawnStudent(this.userId, lab.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to submit.");
		}
    var validKeys = [
      'collaborationQuality',
      'teamworkPositive',
      'teamworkNegative',
      'teamworkSelfImprovement',
      'teamworkGradeCompensation',
      'labPositive',
      'labNegative',
    ]
    if (!_.contains(validKeys, key)) {
      throw new Meteor.Error("invalid-key", "You can't set a value for the given key '"+key+"'.");
    }
		// we log this on purpose, so if the database breaks down, we still have all feedbacks in the log
		console.log("Meteor.methods.setLabFeedback: labId:", lab._id, "userId:", this.userId, "date:", now, "key:", key, "value:", value);
    var data = {
      courseId: lab.courseId,
      labId: labId,
      userId: this.userId,
      modificationDate: now,
    };
    data[key] = value;
    console.log("data:", data);
		var retVal = LabFeedbacks.update({
			labId: labId,
			userId: this.userId,
		}, {
			$set: data,
			$push: {
				log: {what: "Saved", when: now, who: this.userId, value: value},
			},
		}, {
			upsert: true,
		});
    console.log("retVal:", retVal);
		var labFeedback = LabFeedbacks.findOne({labId: labId, userId: this.userId});
    console.log("labFeedback:", labFeedback);
		FeedEvents.insert({userId: this.userId, courseId: lab.courseId, date: now, active: true, event: "ISubmittedALabFeedback", labFeedbackId: labFeedback._id});
		return labFeedback._id;
  },
});
