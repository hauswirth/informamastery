Meteor.methods({
	createCourse: function(abbreviation, instance, title) {
		console.log("Meteor.methods.createCourse("+abbreviation+", "+instance+", "+title+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to create a course.");
		}
		var course = Courses.findOne({abbreviation: abbreviation, instance: instance});
		if (course) {
			throw new Meteor.Error("course-already-exist", "A course with the given abbreviation and instance already exists.");
		}
		if (!isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to create a course.");
		}
		var now = new Date();
		return Courses.insert({
			abbreviation: abbreviation,
			instance: instance,
			title: title,
			visible: false,
			openForRegistration: false,
			ended: false,
			startDate: now,
			endDate: now,
			masteryChecksDetermineGrade: true,
			maxTopicsPerMasteryCheck: 4,
			log: [{what: "Created", when: now, who: this.userId}],
		});
	},
	deleteCourse: function(courseId) {
		console.log("Meteor.methods.deleteCourse("+courseId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to delete a course.");
		}
		var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
		if (!isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to delete a course.");
		}
		var i = InstructorRegistrations.find({courseId: course._id}).count();
		var a = AssistantRegistrations.find({courseId: course._id}).count();
		var s = StudentRegistrations.find({courseId: course._id}).count();
		if (i+a+s>0) {
			throw new Meteor.Error("course-has-registrations", "You cannot delete a course that has registrations.");
		}
		var now = new Date();
		return Courses.update({_id: courseId}, {
			deleted: true,
			deletionDate: now,
			deletedBy: this.userId,
			original: course,
		});
	},
	showCourse: function(courseId) {
		console.log("Meteor.methods.showCourse("+courseId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit the course.");
		}
		var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
		if (!isInstructor(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to change course visibility.");
		}
		var now = new Date();
		return Courses.update({_id: courseId}, {
			$set: {
				visible: true,
			},
			$push: {
				log: {what: "SetVisibilityToTrue", when: now, who: this.userId},
			},
		});
	},
	hideCourse: function(courseId) {
		console.log("Meteor.methods.hideCourse("+courseId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit the course.");
		}
		var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
		if (!isInstructor(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to change course visibility.");
		}
		var now = new Date();
		return Courses.update({_id: courseId}, {
			$set: {
				visible: false,
			},
			$push: {
				log: {what: "SetVisibilityToFalse", when: now, who: this.userId},
			},
		});
	},
	updateSyllabus: function(courseId, syllabus) {
		console.log("Meteor.methods.updateSyllabus("+courseId+", "+syllabus+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit the syllabus.");
		}
		var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
		if (!isInstructor(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to edit this syllabus.");
		}
		var now = new Date();
		return Courses.update({_id: courseId}, {
			$set: {
				syllabus: syllabus,
			},
			$push: {
				log: {what: "Updated", when: now, who: this.userId},
			},
		});
	},
	updateBookListIntroduction: function(courseId, bookListIntroduction) {
		console.log("Meteor.methods.updateBookListIntroduction("+courseId+", "+bookListIntroduction+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit the book list introduction.");
		}
		var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
		if (!isInstructor(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to edit this book list introduction.");
		}
		var now = new Date();
		return Courses.update({_id: courseId}, {
			$set: {
				bookListIntroduction: bookListIntroduction,
			},
			$push: {
				log: {what: "Updated", when: now, who: this.userId},
			},
		});
	},
	updateLabListIntroduction: function(courseId, labListIntroduction) {
		console.log("Meteor.methods.updateLabListIntroduction("+courseId+", "+labListIntroduction+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit the lab list introduction.");
		}
		var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
		if (!isInstructor(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to edit this lab list introduction.");
		}
		var now = new Date();
		return Courses.update({_id: courseId}, {
			$set: {
				labListIntroduction: labListIntroduction,
			},
			$push: {
				log: {what: "Updated", when: now, who: this.userId},
			},
		});
	},
	updateQuizListIntroduction: function(courseId, quizListIntroduction) {
		console.log("Meteor.methods.updateQuizListIntroduction("+courseId+", "+quizListIntroduction+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit the quiz list introduction.");
		}
		var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
		if (!isInstructor(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to edit this quiz list introduction.");
		}
		var now = new Date();
		return Courses.update({_id: courseId}, {
			$set: {
				quizListIntroduction: quizListIntroduction,
			},
			$push: {
				log: {what: "Updated", when: now, who: this.userId},
			},
		});
	},
	updateClassSessionListIntroduction: function(courseId, classSessionListIntroduction) {
		console.log("Meteor.methods.updateClassSessionListIntroduction("+courseId+", "+classSessionListIntroduction+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit the class session list introduction.");
		}
		var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
		if (!isInstructor(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to edit this class session list introduction.");
		}
		var now = new Date();
		return Courses.update({_id: courseId}, {
			$set: {
				classSessionListIntroduction: classSessionListIntroduction,
			},
			$push: {
				log: {what: "Updated", when: now, who: this.userId},
			},
		});
	},
	updateCourse: function(courseId, abbreviation, instance, title, description, useMasteryChecks, masteryChecksDetermineGrade, maxTopicsPerMasteryCheck, useQuizzes, visible, openForRegistration, ended, logoUrl, startDate, endDate, overview) {
		console.log("Meteor.methods.updateCourse("+courseId+", "+abbreviation+", "+instance+", "+title+", "+description+","+useMasteryChecks+","+masteryChecksDetermineGrade+", "+maxTopicsPerMasteryCheck+", "+useQuizzes+", "+visible+", "+openForRegistration+", "+ended+", "+logoUrl+", "+startDate+", "+endDate+", "+overview+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit the lab list introduction.");
		}
		var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
		if (!isInstructor(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to edit this lab list introduction.");
		}
		if (Courses.findOne({_id: {$ne: courseId}, abbreviation: abbreviation, instance: instance})) {
			throw new Meteor.Error("course-already-exist", "A different course with the given abbreviation and instance already exists. You can't modify the abbreviation and instance of this course to an abbreviation/instance combination that already exists in another course.");
		}
		if (maxTopicsPerMasteryCheck<1 || maxTopicsPerMasteryCheck>20) {
			throw new Meteor.Error("max-topics-per-mastery-check-out-of-range", "The maximum number of topics per mastery check needs to be greater than 0 and less than or equal to 20.");
		}
		var now = new Date();
		return Courses.update({_id: courseId}, {
			$set: {
				abbreviation: abbreviation,
				instance: instance,
				title: title,
				description: description,
				useMasteryChecks: useMasteryChecks,
				masteryChecksDetermineGrade: masteryChecksDetermineGrade,
				maxTopicsPerMasteryCheck: maxTopicsPerMasteryCheck,
				useQuizzes: useQuizzes,
				visible: visible,
				openForRegistration: openForRegistration,
				ended: ended,
				logoUrl: logoUrl,
				startDate: startDate,
				endDate: endDate,
				overview: overview,
			},
			$push: {
				log: {what: "Updated", when: now, who: this.userId},
			},
		});
	},
	addSkill: function(topicId) {
		console.log("Meteor.methods.addSkill("+topicId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit a topic.");
		}
		var topic = Topics.findOne({_id: topicId});
		if (!topic) {
			throw new Meteor.Error("topic-does-not-exist", "The specified topic does not exist.");
		}
		if (!isInstructor(this.userId, topic.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to edit this topic.");
		}
		var now = new Date();
		return Skills.insert({
			topicId: topic._id,
			themeId: topic.themeId,
			courseId: topic.courseId,
			log: [{what: "Created", when: now, who: this.userId}],
		});
	},
	updateSkill: function(skillId, title, description) {
		console.log("Meteor.methods.updateSkill("+skillId+", "+title+", "+description+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit a topic.");
		}
		var skill = Skills.findOne({_id: skillId});
		if (!skill) {
			throw new Meteor.Error("skill-does-not-exist", "The specified skill does not exist.");
		}
		if (!isInstructor(this.userId, skill.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to edit this skill.");
		}
		var now = new Date();
		return Skills.update({_id: skillId}, {
			$set: {
				title: title,
				description: description,
			},
			$push: {
				log: {what: "Updated", when: now, who: this.userId},
			},
		});
	},
	removeSkill: function(skillId) {
		console.log("Meteor.methods.removeSkill("+skillId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit a topic.");
		}
		var skill = Skills.findOne({_id: skillId});
		if (!skill) {
			throw new Meteor.Error("skill-does-not-exist", "The specified skill does not exist.");
		}
		if (!isInstructor(this.userId, skill.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to edit this topic.");
		}
		var now = new Date();
		// don't delete, but remove reference to skill's topic (this skill becomes an orphan, and we could reconnect it again)
		return Skills.update({_id: skillId}, {$set: {
			topicId: undefined,
			log: [{what: "Removed", when: now, who: this.userId}],
		}});
	},
	addStudyTask: function(topicId) {
		console.log("Meteor.methods.addStudyTask("+topicId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit a topic.");
		}
		var topic = Topics.findOne({_id: topicId});
		if (!topic) {
			throw new Meteor.Error("topic-does-not-exist", "The specified topic does not exist.");
		}
		if (!isInstructor(this.userId, topic.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to edit this topic.");
		}
		var now = new Date();
		return StudyTasks.insert({
			topicId: topic._id,
			themeId: topic.themeId,
			courseId: topic.courseId,
			log: [{what: "Created", when: now, who: this.userId}],
		});
	},
	updateStudyTask: function(studyTaskId, title, description, kind, duration, body, detail) {
		console.log("Meteor.methods.updateStudyTask("+studyTaskId+", "+title+", "+description+", "+kind+", "+duration+", "+body+", "+detail+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit a topic.");
		}
		var studyTask = StudyTasks.findOne({_id: studyTaskId});
		if (!studyTask) {
			throw new Meteor.Error("study-task-does-not-exist", "The specified study task does not exist.");
		}
		if (!isInstructor(this.userId, studyTask.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to edit this study task.");
		}
		var now = new Date();
		return StudyTasks.update({_id: studyTaskId}, {
			$set: {
				title: title,
				description: description,
				kind: kind,
				duration: duration,
				body: body,
				detail: detail,
			},
			$push: {
				log: {what: "Updated", when: now, who: this.userId},
			},
		});
	},
	removeStudyTask: function(studyTaskId) {
		console.log("Meteor.methods.removeStudyTask("+studyTaskId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit a topic.");
		}
		var studyTask = StudyTasks.findOne({_id: studyTaskId});
		if (!studyTask) {
			throw new Meteor.Error("study-task-does-not-exist", "The specified study task does not exist.");
		}
		if (!isInstructor(this.userId, studyTask.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to edit this topic.");
		}
		var now = new Date();
		// don't delete, but remove reference to studyTask's topic (this studyTask becomes an orphan, and we could reconnect it again)
		return StudyTasks.update({_id: studyTaskId}, {$set: {
			topicId: undefined,
			log: [{what: "Removed", when: now, who: this.userId}],
		}});
	},
});
