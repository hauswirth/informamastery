// MongoDB Collections as defined in our data model:
// https://docs.google.com/drawings/d/1QWVwdzMBQYbUkQxqB4FZvEJoXVSskP3ck1vOCMKF7dc/edit

//--- collections defining courses (not containing any student information)
Courses = new Meteor.Collection('courses');
Themes = new Meteor.Collection('themes');
Topics = new Meteor.Collection('topics');
Skills = new Meteor.Collection('skills');
Misconceptions = new Meteor.Collection('misconceptions');
StudyTasks = new Meteor.Collection('studyTasks');
Labs = new Meteor.Collection('labs');
ContentItems = new Meteor.Collection('contentItems');
Books = new Meteor.Collection('books');
Chapters = new Meteor.Collection('chapters');
Sections = new Meteor.Collection('sections');
Quizzes = new Meteor.Collection('quizzes');
Rubrics = new Meteor.Collection('rubrics');
RubricCategories = new Meteor.Collection('rubricCategories');
RubricItems = new Meteor.Collection('rubricItems');
ClassSessions = new Meteor.Collection('classSessions');

//--- collections containing student information
// registration
InstructorRegistrations = new Meteor.Collection('instructorRegistrations');
AssistantRegistrations = new Meteor.Collection('assistantRegistrations');
StudentRegistrations = new Meteor.Collection('studentRegistrations');

BuddyPreferences = new Meteor.Collection('buddyPreferences');

LabTeams = new Meteor.Collection('labTeams');

// annotations (bookmarks, tags, notes, questions)
ContentItemQuestions = new Meteor.Collection('contentItemQuestions');
// new: let's call them "Annotations" (instead of "ContentIntemQuestions"),
// because a) they are not just for questions, and b) they are not just for ContentItems.
Annotations = new Meteor.Collection("annotations");
AnnotationResponses = new Meteor.Collection("annotationResponses");

// status (current mastery state for a student of a topic/skill, scheduling for checks)
TopicStatus = new Meteor.Collection('topicStatus');
SkillStatus = new Meteor.Collection('skillStatus');
SectionStatus = new Meteor.Collection('sectionStatus');
StudyTaskStatus = new Meteor.Collection('studyTaskStatus');

// submissions for interactive content items (e.g., question)
ContentItemSubmissions = new Meteor.Collection('contentItemSubmissions');
ContentItemScores = new Meteor.Collection('contentItemScores');
LabFeedbacks = new Meteor.Collection('labFeedbacks');

// mastery checks
MasteryCheckSessions = new Meteor.Collection('masteryCheckSessions');
MasteryCheckQueueEntries = new Meteor.Collection('masteryCheckQueueEntries');
MasteryCheckRegistrations = new Meteor.Collection('masteryCheckRegistrations');
MasteryChecks = new Meteor.Collection('masteryChecks');
TopicChecks = new Meteor.Collection('topicChecks');
SkillChecks = new Meteor.Collection('skillChecks');

// pomodoros
Pomodoros = new Meteor.Collection('pomodoros');

// feed
FeedEvents = new Meteor.Collection('feedEvents');

// quiz attempt
QuizAttempts = new Meteor.Collection('quizAttempts');


//--- collections containing student-generated contents that is visible to other students
PracticeProblems = new Meteor.Collection('practiceProblems');
PracticeProblemSolutions = new Meteor.Collection('practiceProblemSolutions');


//--- summaries
Summaries = new Meteor.Collection('summaries');


//--- learning analytics collections
//    this will NOT be published
//    it will be used for research (via MongoDB queries on the server)
AnalyticsLog = new Meteor.Collection('analyticsLog');
