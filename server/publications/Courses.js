// Publish all not-deleted courses
// this is needed even by clients who are not logged in,
// to see all courses in the UI
Meteor.publish("courses", function() {
  var now = new Date();
  var userId = this.userId;
  logPublication(now, userId, "courses", arguments);
  return Courses.find({deleted: {$exists: false}});
});

// Publish all deleted courses
// this is needed by admins, e.g., to undelete a course
Meteor.publish("deleted-courses", function() {
  var now = new Date();
  var userId = this.userId;
  logPublication(now, userId, "deleted-courses", arguments);
  if (isAdmin(userId)) {
    return Courses.find({deleted: {$exists: true}});
  } else {
    return [];
  }
});

// Publish all non-user-specific information of a course
// this is needed even by clients who are not logged in,
// to see the course information (incl. topics and everything)
Meteor.publish("course-contents", function(courseId) {
  var now = new Date();
  var userId = this.userId;
  logPublication(now, userId, "course-contents", arguments);
  var labs;
  var quizzes;
  var themes;
  var topics;
  var classSessions;
  if (isOnTeachingTeam(userId, courseId) || isAdmin(userId)) {
    labs = Labs.find({courseId: courseId});
    quizzes = Quizzes.find({courseId: courseId, removed: false}),
    themes = Themes.find({courseId: courseId});
    topics = Topics.find({courseId: courseId});
    classSessions = ClassSessions.find({courseId: courseId});
  } else {
    labs = Labs.find({courseId: courseId, status: {$ne: "hidden"}});
    quizzes = Quizzes.find({courseId: courseId, removed: false, status: {$ne: "hidden"}}),
    themes = Themes.find({courseId: courseId, status: {$ne: "hidden"}});
    topics = Topics.find({courseId: courseId, status: {$ne: "hidden"}});
    classSessions = ClassSessions.find({courseId: courseId, removed: false, status: {$ne: "hidden"}});
  }
  return [
    Courses.find({_id: courseId}),
    themes,
    topics,
    Skills.find({courseId: courseId}),
    Misconceptions.find({courseId: courseId}),
    StudyTasks.find({courseId: courseId}),
    labs,
    ContentItems.find({courseId: courseId}),
    Books.find({courseId: courseId, removed: false}),
    Chapters.find({courseId: courseId, removed: false}),
    Sections.find({courseId: courseId, removed: false}),
    quizzes,
    Rubrics.find({courseId: courseId, removed: false}),
    RubricCategories.find({courseId: courseId, removed: false}),
    RubricItems.find({courseId: courseId, removed: false}),
    classSessions,
    MasteryCheckSessions.find({courseId: courseId}),
    MasteryCheckQueueEntries.find({courseId: courseId}),
  ];
});

// Publish all my own stuff that is specific to a course
Meteor.publish("my-user-course-contents", function(courseId) {
  var now = new Date();
  var userId = this.userId;
  logPublication(now, userId, "my-user-course-contents", arguments);
  var cursors = [];
  if (isRegisteredAsAnything(userId, courseId)) {
    // only for logged in users who are subscribed as students
    cursors.push(AssistantRegistrations.find({courseId: courseId}));
    cursors.push(InstructorRegistrations.find({courseId: courseId}));

    // Find ids of all users somehow registered in this course
    // Note: This is a non-reactive join.
    //       Code using this will not be updated if e.g., a new user registers.
    //       Because this is used for deciding which users to publish,
    //       client code trying to show information about all subscribed users
    //       will not automatically receive the user documents of new subscribers.
    var studentIds = StudentRegistrations.find({courseId: courseId}).map(function(studentRegistration) {
      return studentRegistration.studentId;
    });
    var assistantIds = AssistantRegistrations.find({courseId: courseId}).map(function(assistantRegistration) {
      return assistantRegistration.assistantId;
    });
    var instructorIds = InstructorRegistrations.find({courseId: courseId}).map(function(instructorRegistration) {
      return instructorRegistration.instructorId;
    });
    var userIds = _.union(studentIds, assistantIds, instructorIds);

    if (isStudent(userId, courseId)) {
      // students see only their own information on the course
      cursors.push(StudentRegistrations.find({courseId: courseId}, {fields: {guerillamailCleartextAddress: 0}})); // exclude other students' guerillamailCleartextAddress
      cursors.push(Annotations.find({courseId: courseId, $or: [{userId: userId}, {kind: "question"}]}));
      cursors.push(ContentItemSubmissions.find({courseId: courseId, userIds: userId}));
      cursors.push(ContentItemScores.find({courseId: courseId, userIds: userId}));
      cursors.push(LabFeedbacks.find({courseId: courseId, userId: userId}));
      cursors.push(TopicStatus.find({courseId: courseId, studentId: userId}));
      cursors.push(SkillStatus.find({courseId: courseId, studentId: userId}));
      ///cursors.push(SectionStatus.find({courseId: courseId, userId: userId}));
      cursors.push(MasteryCheckRegistrations.find({courseId: courseId, studentIds: userId})); // allowing team-based checks: studentIds is an array
      cursors.push(MasteryChecks.find({courseId: courseId, $or: [{studentId: userId}, {studentIds: userId}]})); // backward-compatibility: "studentId" is a scalar (old), "studentIds" is an array (new: allows team-based checks)
      cursors.push(TopicChecks.find({courseId: courseId, $or: [{studentId: userId}, {studentIds: userId}]}));
      cursors.push(SkillChecks.find({courseId: courseId, $or: [{studentId: userId}, {studentIds: userId}]}));
      if (isApprovedStudent(userId, courseId)) {
        // show info about users registered in course ONLY to approved students
        cursors.push(Meteor.users.find({_id: {$in: userIds}}, {fields: {profile: true, status: true, emails: true}}));
      }
      cursors.push(BuddyPreferences.find({courseId: courseId, userId: userId}));
      cursors.push(Pomodoros.find({courseId: courseId, userId: userId}));
      cursors.push(QuizAttempts.find({courseId: courseId, studentId: userId}));
      //cursors.push(FeedEvents.find({courseId: courseId, userId: userId}));
    } else if (isOnTeachingTeam(userId, courseId) || isAdmin(userId)) {
      // teaching team members/admins see information for all users of the course
      cursors.push(StudentRegistrations.find({courseId: courseId}));
      cursors.push(Annotations.find({courseId: courseId}));
      cursors.push(ContentItemSubmissions.find({courseId: courseId}));
      cursors.push(ContentItemScores.find({courseId: courseId}));
      cursors.push(LabFeedbacks.find({courseId: courseId}));
      cursors.push(TopicStatus.find({courseId: courseId}));
      cursors.push(SkillStatus.find({courseId: courseId}));
      ///cursors.push(SectionStatus.find({courseId: courseId}));
      cursors.push(MasteryCheckRegistrations.find({courseId: courseId}));
      cursors.push(MasteryChecks.find({courseId: courseId}));
      cursors.push(TopicChecks.find({courseId: courseId}));
      cursors.push(SkillChecks.find({courseId: courseId}));
      cursors.push(Meteor.users.find({_id: {$in: userIds}}, {fields: {profile: true, status: true, emails: true, communityKnowledge: true}}));
      cursors.push(BuddyPreferences.find({courseId: courseId}));
      cursors.push(Pomodoros.find({courseId: courseId}));
      cursors.push(QuizAttempts.find({courseId: courseId}));
      //cursors.push(FeedEvents.find({courseId: courseId}));
      cursors.push(Summaries.find({courseId: courseId}));
    }

    // practice problem stuff -- to be refined
    cursors.push(PracticeProblems.find({courseId: courseId}));
    cursors.push(PracticeProblemSolutions.find({courseId: courseId}));

    cursors.push(AnnotationResponses.find({courseId: courseId}));

    cursors.push(LabTeams.find({courseId: courseId}));
  }
  return cursors;
});
