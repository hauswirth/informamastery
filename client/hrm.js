// For testing, we want a Collection.
// Just a local collection on the client-side should do.
var Hrm = new Mongo.Collection(null);

// For testing D3, we need a collection with some data points
var Points = new Meteor.Collection(null);
// and a handle for the observer
var Observe;

if (Points.find({}).count()===0) {
	for (i = 0; i < 3000; i++) {
		Points.insert({
			x: Math.floor(Math.random()*1000),
			y: Math.floor(Math.random()*1000)
		});
	}
}

Template.hrm.onRendered(function() {
	console.log("Template.hrm.onRendered:");
	console.log("this.$('#testviz'):", this.$('.testviz'));
	console.log("testviz: initially create the visualization");
	//Width and height
	var w = 800;
	var h = 300;
	var padding = 50;

	//Create scale functions
	var xScale = d3.scale.linear()
		.range([padding, w - padding * 2]);

	var yScale = d3.scale.linear()
		.range([h - padding, padding]);

	//Define X axis
	var xAxis = d3.svg.axis()
		.scale(xScale)
		.orient("bottom")
		.ticks(5);

	//Define Y axis
	var yAxis = d3.svg.axis()
		.scale(yScale)
		.orient("left")
		.ticks(5);

	//Create SVG element
	var svg = d3.select("#testviz")
		.attr("width", w)
		.attr("height", h);

	//Create X axis
	svg.append("g")
		.attr("class", "x axis")
		.attr("transform", "translate(0," + (h - padding) + ")");

	//Create Y axis
	svg.append("g")
		.attr("class", "y axis")
		.attr("transform", "translate(" + padding + ",0)");

	//declare a transistion function to animate position and scale changes
	var transition = function() {
		var maxY = _.first(Points.find({}, {fields:{y: 1}, sort:{y: -1}, limit: 1}).fetch());
		var maxX = _.first(Points.find({}, {fields:{x: 1}, sort:{x: -1}, limit: 1}).fetch());

		//Update scale domains
		xScale.domain([0, maxX.x]);
		yScale.domain([0, maxY.y]);

		//Update X axis
		svg.select(".x.axis")
			.transition()
			.duration(1000)
			.call(xAxis);

		//Update Y axis
		svg.select(".y.axis")
			.transition()
			.duration(1000)
			.call(yAxis);

		svg
			.selectAll("circle")
			.transition()
			.duration(1000)
			.attr("cx", function(d) {
				return xScale(d.x);
			})
			.attr("cy", function(d) {
				return yScale(d.y);
			});
	};

	//transistion once right away to set the axis scale
	transition();

	//decalare a debounced version, only runs once per batch of calls, 50ms after last call - if a bunch of update occur at the same time, when only want to transisition once
	var lazyTransition = _.debounce(transition, 50);

	Observe = Points.find({}, {fields: {x: 1,y: 1}}).observe({
		added: function (document) {
			svg
				.append("circle")
				.datum(document) //attach data
				.attr("cx", function(d) { //set the points right away, even if scale is wrong
					return xScale(d.x);
				})
				.attr("cy", function(d) {
					return yScale(d.y);
				})
				.attr("r", 2)
				.attr("id", "point-"+document._id); //set an id - FYI d3 does not like ids that start with digits
			lazyTransition(); //transition
		},
		changed: function (newDocument, oldDocument) {
			svg
				.select("#point-"+newDocument._id)
				.datum(newDocument); //update data
			lazyTransition(); //transition
		},
		removed: function (oldDocument) {
			svg
				.select("#point-"+oldDocument._id)
				.remove(); //remove element
			lazyTransition(); //transition
		}
	});
});

Template.hrm.onCreated(function() {
	console.log("Template.hrm.onCreated:");
});

Template.hrm.onDestroyed(function() {
	console.log("Template.hrm.onDestroyed:");
	if (Observe) {
		Observe.stop(); //stop the query when the template is destroyed
	}
});

Template.hrm.helpers({
	location: function() {
		return window.location.href;
	},
	protocol: function() {
		return window.location.protocol;
	},
	secure: function() {
		return window.location.protocol==="https:"?"true":"false";
	},
	gravatarUrl: function() {
		//return Gravatar.imageUrl("Matthias.Hauswirth@gmail.com", {secure: window.location.protocol==="https:"});
		//return Gravatar.imageUrl("Matthias.Hauswirth@usi.ch", {secure: window.location.protocol==="https:"});
		if (Meteor.user()) {
			var email = Meteor.user().emails[0].address;
			return Gravatar.imageUrl(email, {size: 128, secure: window.location.protocol==="https:"});
		} else {
			return "";
		}
	},
	getHrm1: function() {
		if (!Hrm.findOne()) {
			Hrm.insert({n: 1, name: "Luca", age: 6});
		}
		return Hrm.findOne({n: 1});
	},
});

Template.hrm.events({
	'click #add-point':function() {
		Points.insert({
			x: Math.floor(Math.random()*1000),
			y: Math.floor(Math.random()*1000)
		});
	},
	'click #remove-point':function() {
		var toRemove = Random.choice(Points.find().fetch());
		Points.remove({_id: toRemove._id});
	},
	'click #randomize-points':function() {
		//loop through bars
		Points.find({}).forEach(function(bar) {
			//update the value of the bar
			Points.update({_id: bar._id}, {$set: {
				x: Math.floor(Math.random()*1000),
				y: Math.floor(Math.random()*1000)
			}});
		});
	},
	'click .hrm-set-name-to-anna-button': function(event, template) {
		console.log("click .hrm-set-name-to-anna-button");
		console.log("event:");
		console.log(event);
		console.log("template:");
		console.log(template);
		console.log("data context:");
		console.log(this);
		Hrm.update({n: 1}, {$set: {name: "Anna"}});
	},
	'click .hrm-set-name-to-luca-button': function(event, template) {
		console.log("click .hrm-set-name-to-luca-button");
		console.log("event:");
		console.log(event);
		console.log("template:");
		console.log(template);
		console.log("data context:");
		console.log(this);
		Hrm.update({n: 1}, {$set: {name: "Luca"}});
	},
	'click .hrm-show-modal-button': function(event, template) {
		console.log("click .hrm-show-modal-button");
		console.log("event:");
		console.log(event);
		console.log("template:");
		console.log(template);
		console.log("data context:");
		console.log(this);
		$('.hrm-modal').modal({
			onShow: function() {
				console.log(".hrm-modal onShow");
			},
			onVisible: function() {
				console.log(".hrm-modal onVisible");
			},
			onHide: function() {
				console.log(".hrm-modal onHide");
			},
			onHidden: function() {
				console.log(".hrm-modal onHidden");
			},
			onApprove: function() {
				console.log(".hrm-modal onApprove");
				console.log("this:");
				console.log(this);
				console.log("$(this)");
				console.log($(this));
				var name = $(this).find(".hrm-field").val();
				Hrm.update({n: 1}, {$set: {name: name}});
			},
			onDeny: function() {
				console.log(".hrm-modal onDeny");
			},
		}).modal("show");
	},
});



Template.hrmModalInner.events({
	'submit .form': function(event, template) {
		console.log("submit .form");
		console.log("event:");
		console.log(event);
		console.log("template:");
		console.log(template);
		console.log("data context:");
		console.log(this);
		// Prevent default form submit
		return false;
	},
	'click .hrm-toggle-button': function(event, template) {
		console.log("click .hrm-toggle-button");
		console.log("event:");
		console.log(event);
		console.log("template:");
		console.log(template);
		console.log("data context:");
		console.log(this);
	},
});
