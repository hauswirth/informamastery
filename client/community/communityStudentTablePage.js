Template.communityStudentTablePage.helpers({
  getStudentCount: function(courseId) {
		return StudentRegistrations.find({courseId: courseId, withdrawn: false}).count();
	},
	findUsers: function(courseId) {
		var userIds = StudentRegistrations.find({courseId: courseId}).map(function(studentRegistration) {return studentRegistration.studentId;});
		return Meteor.users.find({_id: {$in: userIds}});
	},
	makeSettings: function(courseId) {
		return {
			rowsPerPage: 50,
			class: "ui compact table",
			fields: [
				{
					key: "gravatar",
					label: "Photo",
					sortable: false,
					tmpl: Template.userGravatar,
					headerClass: "collapsing",
				},
				{
					key: "profile.firstName",
					label: "First",
					sort: "ascending",
					tmpl: Template.studentFirstName,
					headerClass: "collapsing",
				},
				{
					key: "profile.lastName",
					label: "Last",
					tmpl: Template.studentLastName,
					headerClass: "collapsing",
				},
				{
					key: "primaryEmail",
					label: "Primary Email",
					tmpl: Template.userPrimaryMail,
					fn: function(value, object) {return object.emails[0].address;},
				},
				{
					key: "approved",
					label: "Approved",
					fn: function(value, object) {
						var studentRegistration = StudentRegistrations.findOne({courseId: courseId, studentId: object._id});
						var approved = studentRegistration?studentRegistration.approved:false;
						return new Spacebars.SafeString(approved?"<i class='green checkmark icon'>":"");
					},
				},
				{
					key: "withdrawn",
					label: "Withdrawn",
					fn: function(value, object) {
						var studentRegistration = StudentRegistrations.findOne({courseId: courseId, studentId: object._id});
						var withdrawn = studentRegistration?studentRegistration.withdrawn:false;
						return new Spacebars.SafeString(withdrawn?"<i class='red checkmark icon'>":"");
					},
				},
			],
		};
	},
});
