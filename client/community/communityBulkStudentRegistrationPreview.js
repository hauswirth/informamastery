Template.communityBulkStudentRegistrationPreview.helpers({
  gravatarUrl: function(email) {
    return Gravatar.imageUrl(email, {size: 50, secure: window.location.protocol==="https:"});
  },
});

Template.communityBulkStudentRegistrationPreview.events({
	'click .bulk-register-button': function(event, template) {
    //console.log(this);
		console.log("click .bulk-register-button");
    //console.log(event);
    //console.log(template);
    var courseId = template.data.courseId;
    var students = template.data.students;
    console.log("courseId:", courseId);
		console.log("students:", students);
    Meteor.call('createUsersAndRegisterAndApproveAsStudents', courseId, students, function(error, success) {
      if (error) {
        throwError(error.reason);
      }
      Router.go("communityStudentTablePage", {courseId: courseId});
    });
  },
});
