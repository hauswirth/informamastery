Template.communityStudentPhotosPage.helpers({
  getStudentCount: function(courseId) {
    console.log("Template.communityStudentPhotosPage.helpers:getStudentCount("+courseId+")");
		return StudentRegistrations.find({courseId: courseId, withdrawn: false}).count();
	},
  getNotWithdrawnStudentregistrations: function(courseId) {
		console.log("Template.communityStudentPhotosPage.helpers:getNotWithdrawnStudentregistrations("+courseId+")");
		return StudentRegistrations.find({courseId: courseId, withdrawn: false});
	},
});
