Template.sectionRecallsPage.helpers({
  countStartedStatuses: function(sectionId) {
		//console.log("countStartedStatuses("+sectionId+")");
		return SectionStatus.find({sectionId: sectionId, startedReading: {$exists: true}}).count();
	},
	countFinishedStatuses: function(sectionId) {
		//console.log("countFinishedStatuses("+sectionId+")");
		return SectionStatus.find({sectionId: sectionId, finishedReading: {$exists: true}}).count();
	},
	countRecalledStatuses: function(sectionId) {
		//console.log("countRecalledStatuses("+sectionId+")");
		return SectionStatus.find({sectionId: sectionId, recallText: {$exists: true}}).count();
	},
	getRecalledStatuses: function(sectionId) {
		//console.log("getRecalledStatuses("+sectionId+")");
		return SectionStatus.find({sectionId: sectionId, recallText: {$exists: true}}, {sort: {modificationDate: -1}});
	},
	getMostRecentlyStartedStatus: function(sectionId) {
		//console.log("getMostRecentlyStartedStatus("+sectionId+")");
		return SectionStatus.findOne({sectionId: sectionId, startedReading: true}, {sort: {startedReadingDate: -1}});
	},
});
