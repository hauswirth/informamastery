Template.chapterPage.helpers({
	countSections: function(chapterId) {
		//console.log("countSections("+chapterId+")");
		return Sections.find({chapterId: chapterId}).count();
	},
	countOwnSectionStatuses: function(chapterId) {
		//console.log("countOwnSectionStatuses("+chapterId+")");
		return SectionStatus.find({chapterId: chapterId, userId: Meteor.userId()}).count();
	},
	getOwnSectionStatus: function(sectionId) {
		//console.log("getOwnSectionStatus("+sectionId+")");
		//console.log(SectionStatus.findOne({sectionId: sectionId, userId: Meteor.userId()}));
		return SectionStatus.findOne({sectionId: sectionId, userId: Meteor.userId()});
	},
	countAllRecalledSectionStatuses: function(sectionId) {
		console.log("countAllRecalledSectionStatuses("+sectionId+")");
		return SectionStatus.find({sectionId: sectionId, modificationDate: {$exists: true}}).count();
	},
	getPreviousChapter: function(chapter) {
		return Chapters.findOne({bookId: chapter.bookId, removed: false, sortKey: {$lt: chapter.sortKey}}, {sort: {sortKey: -1}});
	},
	getNextChapter: function(chapter) {
		return Chapters.findOne({bookId: chapter.bookId, removed: false, sortKey: {$gt: chapter.sortKey}}, {sort: {sortKey: 1}});
	},
	getTopicsForChapterId: function(chapterId) {
		console.log("getTopicsForChapterId("+chapterId+")");
		var sectionIds = Sections.find({chapterId: chapterId, removed: false}).map(function(section) {return section._id;});
		console.log("sectionIds", sectionIds);
		var topicIds = StudyTasks.find({"detail.sectionIds": {$in: sectionIds}}).map(function(studyTask) {return studyTask.topicId});
		console.log("topicIds", topicIds);
		return Topics.find({_id: {$in: topicIds}}, {sort: {masteryRequiredForGrade: 1}});
	},
});
