Template.labItemGradingPage.helpers({
  getTeams: function(labId) {
    if (labId) {
      return LabTeams.find({labId: labId, removed: {$ne: true}}, {sort: {creationDate: 1}});
    } else {
      return undefined;
    }
  },
  getTeamSubmission: function(teamId, itemId) {
		console.log("getTeamSubmission("+teamId+", "+itemId+")");
		return ContentItemSubmissions.findOne({itemId: itemId, labTeamId: teamId});
	},
  getReferenceSubmission: function(itemId) {
		console.log("getReferenceSubmission("+itemId+")");
		return ContentItemSubmissions.findOne({itemId: itemId, labTeamId: null});
	},
  getLabSubmissionFiles: function(teamId, itemId) {
    return LabSubmissionFiles.find({"metadata.labItemId": itemId, "metadata.labTeamId": teamId});
  },
  countLabSubmissionFiles: function(teamId, itemId) {
    return LabSubmissionFiles.find({"metadata.labItemId": itemId, "metadata.labTeamId": teamId}).count();
  },
  getLabSubmissionImageFiles: function(teamId, itemId) {
    return LabSubmissionFiles.find({"metadata.labItemId": itemId, "metadata.labTeamId": teamId, $or: [{contentType: "image/jpeg"}, {contentType: "image/png"}, {contentType: "image/gif"}]});
  },
  countLabSubmissionImageFiles: function(teamId, itemId) {
    return LabSubmissionFiles.find({"metadata.labItemId": itemId, "metadata.labTeamId": teamId, $or: [{contentType: "image/jpeg"}, {contentType: "image/png"}, {contentType: "image/gif"}]}).count();
  },
  link: function() {
    //console.log('labSubmissionFileList.helpers.link');
    return LabSubmissionFiles.baseURL + "/md5/" + this.md5;
  },
  shortFilename: function(w) {
    //console.log('labSubmissionFileList.helpers.shortFilename');
    if (!w) {
      w = 16;
    }
    if (this.filename && this.filename.length) {
      return shortenFileName(this.filename, w);
    } else {
      return "(no filename)";
    }
  },
  owner: function() {
    return this.metadata ? this.metadata.ownerId : undefined;
  },
  formattedLength: function() {
    return numeral(this.length).format('0.0b');
  },
});

// HACK: Just copied this from labSubmissionFileList
function shortenFileName(name, w) {
  if (!w) {
    w = 16;
  }
  w += w % 4;
  w = (w-4)/2
  if (name.length > 2*w) {
    return name.substring(0, w) + '…' + name.substring(name.length-w, name.length);
  } else {
    return name;
  }
}
