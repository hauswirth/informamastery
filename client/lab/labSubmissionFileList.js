Template.labSubmissionFileList.helpers({
  getLabSubmissionFiles: function() {
    //console.log("labSubmissionFileList:getLabSubmissionFiles: this:", this);
    var labTeam = LabTeams.findOne({labId: this.containerId, memberIds: Meteor.userId(), removed: {$ne: true}});
    if (labTeam) {
      return LabSubmissionFiles.find({"metadata.labItemId": this._id, "metadata.labTeamId": labTeam._id});
    } else {
      return null;
    }
    //return LabSubmissionFiles.find({"metadata.labItemId": this._id, "metadata.userIds": Meteor.userId()});
  },
  labOpen: function() {
    //console.log("labSubmissionFileList:labOpen");
    //console.log("this:", this);
    var lab = Labs.findOne({_id: this.metadata.labId});
    //console.log("lab:", lab);
    return lab && lab.status=="open";
  },
  link: function() {
    //console.log('labSubmissionFileList.helpers.link');
    return LabSubmissionFiles.baseURL + "/md5/" + this.md5;
  },
  owner: function() {
    return this.metadata ? this.metadata.ownerId : undefined;
  },
  formattedLength: function() {
    return numeral(this.length).format('0.0b');
  },
  uploadStatus: function() {
    var percent = Session.get(""+this._id);
    if (percent) {
      return "Uploading...";
    } else {
      return "Processing...";
    }
  },
  uploadProgress: function() {
    var percent = Session.get(""+this._id);
    return percent;
  },
});

Template.labSubmissionFileList.events({
  'click .del-file': function(e, t) {
    // Just the remove method does it all
    LabSubmissionFiles.remove({_id: this._id});
  },
});

function shortenFileName(name, w) {
  if (!w) {
    w = 16;
  }
  w += w % 4;
  w = (w-4)/2
  if (name.length > 2*w) {
    return name.substring(0, w) + '…' + name.substring(name.length-w, name.length);
  } else {
    return name;
  }
}
