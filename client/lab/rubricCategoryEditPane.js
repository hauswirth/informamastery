Template.rubricCategoryEditPane.helpers({
  getRubricItems: function(rubricCategoryId) {
    return RubricItems.find({rubricCategoryId: rubricCategoryId, removed: {$ne: true}}, {sort: {index: 1}});
  },
});

Template.rubricCategoryEditPane.events({
  'click .configure-rubric-category-button': function(event, template) {
		console.log("click .configure-rubric-category-button");
		var category = this;
    console.log(category);
    var modalSelector = ".rubric-category-edit-modal."+category._id;
    console.log(modalSelector);
    console.log("modal:", $(modalSelector));
    // NOTE: Calling .modal() on this will remove it from its current place in the DOM
    //       and place it somewhere else (near the end of the HTML) in the DOM.
    //       Thus, it would be impossible to find it starting from "template.$(...)",
    //       the second time we call this.
    //       So we use a global JQuery "$(...)"
    //       and add a the category ID as a class to the modal to uniquely identify it.
    $(modalSelector).modal({
      onShow: function() {
        console.log("rubric-category-edit-modal onShow()");
        console.log("category:", category);
        //console.log("this", this);
        //console.log("$(this)", $(this));
        //console.log("$(modalSelector)", $(modalSelector));
        //console.log("radio via this:         ", $(this).find(".ui.checkbox"));
        //console.log("radio via modalSelector:", $(modalSelector).find(".ui.checkbox"));
        var jModal = $(this);
        jModal.find(".category-name-input").val(category.name);
        jModal.find(".category-points-input").val(category.maxPoints);
        jModal.find(".ui.checkbox.category-additive").checkbox({
          onChecked: function() {
            console.log("category-additive onChecked");
            jModal.find(".ui.checkbox.category-subtractive").checkbox("uncheck");
          }
        });
        jModal.find(".ui.checkbox.category-subtractive").checkbox({
          onChecked: function() {
            console.log("category-subtractive onChecked");
            jModal.find(".ui.checkbox.category-additive").checkbox("uncheck");
          }
        });
        jModal.find(".ui.checkbox.category-enable-bonus-malus").checkbox({
          onChange: function() {
            console.log("category-enable-bonus-malus onChange");
          }
        });
        // initialize controls to good values
        if (category.additive) {
          jModal.find(".ui.checkbox.category-additive").checkbox("check");
        } else {
          jModal.find(".ui.checkbox.category-subtractive").checkbox("check");
        }
        if (category.enableBonusMalus) {
          jModal.find(".ui.checkbox.category-enable-bonus-malus").checkbox("check");
        }
      },
      onDeny: function(){
        console.log("rubric-category-edit-modal onDeny()");
        console.log(this);
        //return false;
      },
      onApprove: function() {
        console.log("rubric-category-edit-modal onApprove()");
        console.log(this);
        var jModal = $(this);
        var name = jModal.find(".category-name-input").val();
        var maxPoints = jModal.find(".category-points-input").val();
        var additive = jModal.find(".ui.checkbox.category-additive").checkbox("is checked");
        var enableBonusMalus = jModal.find(".ui.checkbox.category-enable-bonus-malus").checkbox("is checked");
        Meteor.call("updateRubricCategory", category._id, name, maxPoints, additive, enableBonusMalus, function(error, result) {
          if (error) {
            throwError(error.reason);
          }
        });
      },
    }).modal('show');

  },
  'click .remove-rubric-category-button': function(event, template) {
    console.log("click .remove-rubric-category-button");
		var rubricCategory = this;
		Meteor.call("removeRubricCategory", rubricCategory._id, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
  },
  'click .add-rubric-item-button': function(event, template) {
		console.log("click .add-rubric-item-button");
		var rubricCategory = this;
		Meteor.call("createRubricItem", rubricCategory._id, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
