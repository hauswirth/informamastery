Template.labSubmissionUploadArea.onRendered(function() {
  //console.log('labSubmissionUploadArea.onRendered');
  //LabSubmissionFiles.resumable.assignDrop($('.fileDrop'));
  //LabSubmissionFiles.resumable.assignBrowse($('.fileBrowse'));
});

Template.labSubmissionUploadArea.helpers({
  labOpen: function() {
    console.log("labSubmissionUploadArea:labOpen");
    console.log("this:", this);
    var lab = Labs.findOne({_id: this.containerId});
    console.log("lab:", lab);
    return lab && lab.status=="open";
  },
});

Template.labSubmissionUploadArea.events({
  'click .uploadButton': function(e, t) {
    console.log("labSubmissionUploadArea: click .uploadButton");
    e.preventDefault();
    e.stopPropagation();
    var input = t.find(".uploadInput");
    console.log("input:", input);
    input.click();
  },
  'change .uploadInput': function(e, t) {
    console.log("labSubmissionUploadArea: change .uploadInput");
    attachFiles(e.target.files, e, t);
    e.target.value = '';
  },
  'dragenter .uploadArea': function(e) {
    console.log("labSubmissionUploadArea: dragenter .uploadArea");
    e.preventDefault();
    e.stopPropagation();
  },
  'dragover .uploadArea': function(e) {
    console.log("labSubmissionUploadArea: dragover .uploadArea");
    e.preventDefault();
    e.stopPropagation();
  },
  'drop .uploadArea': function(e, t) {
    console.log("labSubmissionUploadArea: drop .uploadArea");
    e.preventDefault();
    e.stopPropagation();
    attachFiles(e.originalEvent.dataTransfer.files, e, t);
  },
});

function attachFiles(files, e, t) {
  console.log("attachFiles(", files, e, t, ")");
  var labItemId = t.data._id;
  console.log("labItemId: ", labItemId);
  var labId = t.data.containerId;
  console.log("labId: ", labId);
  var labTeam = LabTeams.findOne({labId: labId, memberIds: Meteor.userId(), removed: {$ne: true}});
  console.log("labTeam:", labTeam);
  _.each(files, function(file) {
    /*
    //This callback does not seem to be called
    file.callback = function(file2) {
      console.log("upload: callback(", file2, ")");
    };
    */
    file.courseId = labTeam.courseId;
    file.labId = labId;
    file.labItemId = labItemId;
    file.labTeamId = labTeam._id;
    file.userIds = labTeam.memberIds;
    LabSubmissionFiles.resumable.addFile(file, e);
    console.log("upload: addFile(", file, ", ", e,")");
  });
}
