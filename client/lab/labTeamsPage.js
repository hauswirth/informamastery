var labId = undefined;

var sortableOptions = {
  connectWith: ".team",
  stop: function(e, ui) {
    console.log("stop.");
    //console.log("ui=", ui, "e=", e);
    // get the dragged html element and the one before and after it
    //console.log("ui.item.get(0)=", ui.item.get(0));
    //console.log("Blaze.getData(ui.item.get(0))=", Blaze.getData(ui.item.get(0)));
    //console.log("ui.item.closest('.team').get(0)", ui.item.closest('.team').get(0));
    var destinationExistingTeam = ui.item.closest('.team.existing-team').get(0);
    //console.log("ui.item.closest('.team.existing-team').get(0)", destinationExistingTeam);
    var destinationUnassignedTeam = ui.item.closest('.team.unassigned-team').get(0);
    //console.log("ui.item.closest('.team.unassigned-team').get(0)", destinationUnassignedTeam);
    var destinationNewTeam = ui.item.closest('.team.new-team').get(0);
    //console.log("ui.item.closest('.team.new-team').get(0)", destinationNewTeam);
    // Blaze.getData takes as a parameter an html element
    // and returns the data context that was bound when that html element was rendered
    //console.log("Blaze.getData(ui.item.closest('.team').get(0))", Blaze.getData(ui.item.closest('.team').get(0)));

    console.log("ui.item.get(0):", ui.item.get(0));
    //var studentRegistration = Blaze.getData(ui.item.get(0));
    //console.log("StudentRegistration:", studentRegistration);
    //var studentId = studentRegistration.studentId;
    var data = Blaze.getData(ui.item.get(0));
    console.log("data:", data);
    var studentId = data.user._id;
    console.log("studentId:", studentId);
    var sourceTeam = LabTeams.findOne({labId: labId, memberIds: studentId, removed: {$ne: true}});
    //console.log("sourceTeam:", sourceTeam);
    if (sourceTeam) {
      // remove student from source team
      Meteor.call('removeLabTeamMember', sourceTeam._id, studentId, function(error, result) {
        if (error) {
          throwError(error.reason);
        }
      });
    }
    if (destinationExistingTeam) {
      console.log("Dropped on existing team:", destinationExistingTeam);
      // add student to the existing team
      var destinationTeam = Blaze.getData(destinationExistingTeam);
      console.log("destinationTeam:", destinationTeam);
      Meteor.call('addLabTeamMember', destinationTeam._id, studentId, function(error, result) {
        if (error) {
          throwError(error.reason);
        }
      });
    } else if (destinationNewTeam) {
      console.log("Dropped on new team");
      // create a new team
      Meteor.call('createLabTeam', labId, function(error, result) {
        if (error) {
          throwError(error.reason);
        }
        console.log("created team:", result);
        // add student to the new team
        Meteor.call('addLabTeamMember', result, studentId, function(error, result) {
          if (error) {
            throwError(error.reason);
          }
        });
        //TODO: The createLabTeam call created a new LabTeam,
        //      which appeared on the page
        //      but for which no sortable() call was executed
        //      (the sortable call only gets executed at initial rendering time)
        //      We now need to trigger such a call,
        //      but not right now, but after the new LabTeam card has actually been rendered
      });
    } else if (destinationUnassignedTeam) {
      console.log("Dropped on unassigned team");
    }
  }
}

Template.labTeamsPage.rendered = function() {
  var self = this;
  console.log("Template.labTeamsPage.rendered");
  console.log("this", this);
  //console.log("this.$('.team')", this.$('.team'));
  //console.log("$('.team')", $('.team'));

  if (self.data.lab) {
    // set labId, so that future sortable(sortableOptions) calls can access it
    labId = self.data.lab._id;
  }
  this.$('.team').sortable(sortableOptions);
};

Template.labTeamsPage.helpers({
	getTeams: function(lab) {
    if (lab) {
      return LabTeams.find({labId: lab._id, removed: {$ne: true}}, {sort: {creationDate: 1}});
    } else {
      return undefined;
    }
  },
  getUnassignedStudentIds: function(lab) {
    var studentIds = StudentRegistrations.find({courseId: lab.courseId, approved: true, withdrawn: false}).map(function(studentRegistration) {return studentRegistration.studentId;});
    var assignedStudentIds = [];
    LabTeams.find({labId: lab._id, removed: {$ne: true}}).forEach(function(labTeam) {
      assignedStudentIds = assignedStudentIds.concat(labTeam.memberIds);
    });
    return _.difference(studentIds, assignedStudentIds);
  },
  reattachSortable: function() {
    console.log("Template.labTeamsPage.helpers:reattachSortable", "this:", this);
    var templateInstance = Template.instance();
    console.log("templateInstance:", templateInstance);
    if (templateInstance && templateInstance.firstNode) {
      templateInstance.$('.team').sortable(sortableOptions);
    }
  },
  getUploadCount: function() {
    console.log("Template.labTeamsPage.helpers:getUploadCount");
    //console.log(this);
    var labTeam = this;
    console.log("labTeam:", labTeam);
    var uploadCount = LabSubmissionFiles.find({"metadata.labTeamId": labTeam._id}).count();
    console.log("uploadCount:", uploadCount);
    return uploadCount;
  },
  getSubmissionCount: function() {
    console.log("Template.labTeamsPage.helpers:getSubmissionCount");
    //console.log(this);
    var labTeam = this;
    return ContentItemSubmissions.find({labTeamId: labTeam._id}).count();
  },
  getPriorLab: function(lab) {
    console.log("getPriorLab(", lab, ")");
    var l = _priorLab(lab);
    console.log(l);
    return l;
  },
});

_priorLab = function(lab) {
  return Labs.findOne({courseId: lab.courseId, removed: {$ne: true}, status: {$ne: "hidden"}, groupSize: lab.groupSize, deadlineDate: {$lt: lab.deadlineDate}}, {sort: {deadlineDate: -1}, limit: 1});
}

Template.labTeamsPage.events({
  'click .copy-previous-lab-assignment-button': function(event, template) {
    console.log("Template.labTeamsPage.events:click .copy-previous-lab-assignment-button", "this:", this);
    var templateInstance = Template.instance();
    console.log("templateInstance:", templateInstance);
    var lab = templateInstance.data.lab;
    //var labId = this.lab._id;
    var priorLab = _priorLab(lab);
    Meteor.call("copyLabTeamsToLabTeams", priorLab._id, lab._id, function(error, result) {
      if (error) {
        throwError(error.reason);
      }
      console.log("created.");
    });
  },
  'click .randomly-assign-unassigned-students-button': function(event, template) {
    console.log("Template.labTeamsPage.events:click .randomly-assign-unassigned-students-button", "this:", this);
    var labId = this.lab._id;
    Meteor.call("randomlyAssignUnassignedStudentsToLabTeams", labId, function(error, result) {
      if (error) {
        throwError(error.reason);
      }
      console.log("created.");
    });
  },
  'click .buddy-assign-unassigned-students-button': function(event, template) {
    console.log("Template.labTeamsPage.events:click .buddy-assign-unassigned-students-button", "this:", this);
    var labId = this.lab._id;
    Meteor.call("buddyAssignUnassignedStudentsToLabTeams", labId, function(error, result) {
      if (error) {
        throwError(error.reason);
      }
      console.log("created.");
    });
  },
  'click .randomly-assign-all-students-button': function(event, template) {
    console.log("Template.labTeamsPage.events:click .randomly-assign-all-students-button", "this:", this);
    var labId = this.lab._id;
    Meteor.call("createRandomLabTeams", labId, function(error, result) {
      if (error) {
        throwError(error.reason);
      }
      console.log("created.");
    });
  },
  'click .remove-team-button': function(event, template) {
    console.log("Template.labTeamsPage.events:click .remove-team-button", "this:", this);
    event.preventDefault();
    event.stopPropagation();
    var labTeam = this;
    Meteor.call('removeLabTeam', labTeam._id, function(error, result) {
      if (error) {
        throwError(error.reason);
      }
      console.log("removed team.");
    });
  },
});
