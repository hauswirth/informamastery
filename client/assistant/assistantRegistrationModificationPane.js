Template.assistantRegistrationModificationPane.events({
  'click .withdraw-button': function(ev, template) {
		console.log("withdraw-button clicked");
		var assistantRegistration = Template.currentData();
		var courseId = assistantRegistration.courseId;
		var assistantId = assistantRegistration.assistantId;
		Meteor.call('withdrawUserAsAssistant', assistantId, courseId, function(error, assistantRegistrationId) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .undo-withdrawal-button': function(ev, template) {
		console.log("undo-withdrawal-button clicked");
    var assistantRegistration = Template.currentData();
		var courseId = assistantRegistration.courseId;
		var assistantId = assistantRegistration.assistantId;
		Meteor.call('undoWithdrawalOfUserAsAssistant', assistantId, courseId, function(error, assistantRegistrationId) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
