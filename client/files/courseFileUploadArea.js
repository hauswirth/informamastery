Template.courseFileUploadArea.events({
  'click .uploadButton': function(e, t) {
    console.log("courseFileUploadArea: click .uploadButton");
    e.preventDefault();
    e.stopPropagation();
    var input = t.find(".uploadInput");
    console.log("input:", input);
    input.click();
  },
  'change .uploadInput': function(e, t) {
    console.log("courseFileUploadArea: change .uploadInput");
    attachFiles(e.target.files, e, t);
    e.target.value = '';
  },
  'dragenter .uploadArea': function(e) {
    console.log("courseFileUploadArea: dragenter .uploadArea");
    e.preventDefault();
    e.stopPropagation();
  },
  'dragover .uploadArea': function(e) {
    console.log("courseFileUploadArea: dragover .uploadArea");
    e.preventDefault();
    e.stopPropagation();
  },
  'drop .uploadArea': function(e, t) {
    console.log("courseFileUploadArea: drop .uploadArea");
    e.preventDefault();
    e.stopPropagation();
    attachFiles(e.originalEvent.dataTransfer.files, e, t);
  },
});

function attachFiles(files, e, t) {
  console.log("attachFiles(", files, e, t, ")");
  var course = t.data;
  console.log("course: ", course);
  console.log("courseId: ", course._id);
  _.each(files, function(file) {
    /*
    //This callback does not seem to be called
    file.callback = function(file2) {
      console.log("upload: callback(", file2, ")");
    };
    */
    file.courseId = course._id;
    CourseFiles.resumable.addFile(file, e);
    console.log("upload: addFile(", file, ", ", e,")");
  });
}
