Template.themePage.helpers({
	getTopicsForTheme: function(themeId) {
		return Topics.find({themeId: themeId, removed: {$ne: true}}, {sort: {masteryRequiredForGrade: 1}} );
	},
});
