_pomodoroDuration = 25; // a pomodoro is supposed to last 25 minutes


Template.pomodoroSegment.helpers({
	countCompletedPomodoros: function() {
		var course = Session.get("course");
		var courseId = course?course._id:undefined;
		return Pomodoros.find({courseId: courseId, userId: Meteor.userId(), completed: true}).count();
	},
	countOngoingPomodoros: function() {
		var course = Session.get("course");
		var courseId = course?course._id:undefined;
		return Pomodoros.find({courseId: courseId, userId: Meteor.userId(), ongoing: true}).count();
	},
	getOngoingPomodoro: function() {
		var course = Session.get("course");
		var courseId = course?course._id:undefined;
		return Pomodoros.findOne({courseId: courseId, userId: Meteor.userId(), ongoing: true});
	},
	getMinutesLeftInOngoingPomodoro: function() {
		var course = Session.get("course");
		var courseId = course?course._id:undefined;
		var pomodoro = Pomodoros.findOne({courseId: courseId, userId: Meteor.userId(), ongoing: true});
		if (pomodoro) {
			var startMoment = moment(pomodoro.startTime);
			var endMoment = moment(TimeSync.serverTime(null, 1000)); // update every 1 seconds
			var durationInMinutes = endMoment.diff(startMoment, 'minutes');
			var minutesLeft = _pomodoroDuration - durationInMinutes;
			return minutesLeft;
		} else {
			return "?";
		}
	},
	getOngoingPomodoroStartTime: function() {
		var course = Session.get("course");
		var courseId = course?course._id:undefined;
		var pomodoro = Pomodoros.findOne({courseId: courseId, userId: Meteor.userId(), ongoing: true});
		if (pomodoro) {
			var m = moment(pomodoro.startTime).format("HH:mm:ss");
			return m;
		} else {
			return undefined;
		}
	},
	getServerTime: function() {
		var time = TimeSync.serverTime();
		var m = moment(time).format("HH:mm:ss");
		return m;
	},
});


Template.pomodoroSegment.events({
	'click .start-pomodoro-button': function(event) {
		console.log("start-pomodoro-button clicked");
		$('.start-pomodoro-modal').modal({
			onShow: function() {
				console.log("start-pomodoro-modal onShow()");
				console.log(this);
				// initialize controls to good values
				$(this).find(".pomodoro-goal-input").val("");
				$(this).find('.topic-dropdown').dropdown();
				$(this).find('.lab-dropdown').dropdown();
				// determine topicId and labId based on current route
				var currentTopicId = undefined;
				var currentLabId = undefined;
				//console.log(Router.current());
				//console.log(Router.current()?Router.current().route:"?");
				//console.log(Router.current().route?Router.current().route.getName():"?");
				console.dir(Router.current()?Router.current().params:"?");
				if (Router.current() && Router.current().params) {
					// the topic-specific routes use a topicId param
					currentTopicId = Router.current().params.topicId;
					// currently, the lab-specific routes don't use a labId param
					// (but once we refactor them, this will pick it up nicely)
					currentLabId = Router.current().params.labId;
				}
				if (currentTopicId) {
					$(this).find('.topic-dropdown').dropdown("set selected", currentTopicId);
				} else {
					$(this).find('.topic-dropdown').dropdown("clear");
				}
				if (currentLabId) {
					$(this).find('.lab-dropdown').dropdown("set selected", currentLabId);
				} else {
					$(this).find('.lab-dropdown').dropdown("clear");
				}
			},
			onDeny: function(){
				console.log("start-pomodoro-modal onDeny()");
				console.log(this);
				//return false;
			},
			onApprove: function() {
				console.log("start-pomodoro-modal onApprove()");
				console.log(this);
				var course = Session.get("course");
				var courseId = course?course._id:undefined;
				var goal = $(this).find(".pomodoro-goal-input").val();
				var topicId = $(this).find(".pomodoro-topic-input").val();
				var labId = $(this).find(".pomodoro-lab-input").val();
				Meteor.call("startPomodoro", goal, courseId, topicId, labId, function(error, result) {
					if (error) {
						throwError(error.reason);
					} else {
						var sound = new Howl({
							urls: ['/sounds/PomodoroTimerStart2.mp3'],
						});
						console.log(sound);
						sound.play();
					}
				});
			},
		}).modal('show');
	},
	'click .abort-pomodoro-button': function(event) {
		console.log("abort-pomodoro-button clicked");
		console.log($('.abort-pomodoro-modal'));
		$('.abort-pomodoro-modal').modal({
			onShow: function() {
				console.log("abort-pomodoro-modal onShow()");
				console.log(this);
				$(this).find(".reason-field").val("");
			},
			onDeny: function(){
				//return false;
			},
			onApprove: function() {
				console.log("abort-pomodoro-modal onApprove()");
				console.log(this);
				var course = Session.get("course");
				var courseId = course?course._id:undefined;
				var pomodoro = Pomodoros.findOne({userId: Meteor.userId(), courseId: courseId, ongoing: true});
				var pomodoroId = pomodoro?pomodoro._id:undefined;
				var reason = $(this).find(".reason-field").val();
				Meteor.call("abortPomodoro", pomodoroId, reason, function(error, result) {
					if (error) {
						throwError(error.reason);
					} else {
					}
				});
			},
		}).modal('show');
	},
});
