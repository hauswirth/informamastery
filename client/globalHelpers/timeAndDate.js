// date and time helpers
Template.registerHelper("formatDate", function(date) {
	return moment(date).format("dddd, MMMM Do YYYY");
});

Template.registerHelper("formatDateShort", function(date) {
	return moment(date).format("ddd, MMM D");
});

Template.registerHelper("formatDateForPickadate", function(date) {
	if (date) {
		return moment(date).format("MMMM D, YYYY");
	} else {
		return undefined;
	}
});

Template.registerHelper("formatDateForPickatime", function(date) {
	if (date) {
		return moment(date).format("HH:mm");
	} else {
		return undefined;
	}
});

Template.registerHelper("formatWeekday", function(date) {
	return moment(date).format("dddd");
});

Template.registerHelper("formatWeekInCourse", function(courseId, date) {
	//console.log("formatWeekInCourse("+courseId+","+date+")");
	var course = Courses.findOne({_id: courseId});
	//console.log("course=");
	//console.log(course);
	var courseStartDate = course.startDate;
	//console.log("courseStartDate=");
	//console.log(courseStartDate);
	//console.log("moment=");
	//console.log(moment);
	var courseStartMoment = moment(courseStartDate);
	//console.log(courseStartMoment);
	var dateMoment = moment(date);
	return dateMoment.diff(courseStartMoment, "weeks")+1;
});

Template.registerHelper("formatTime", function(date) {
	return moment(date).format("HH:mm:ss");
});

Template.registerHelper("formatTimeHoursMinutes", function(date) {
	return moment(date).format("HH:mm");
});

Template.registerHelper("formatDateTime", function(date) {
	return moment(date).format("dddd, MMMM Do YYYY HH:mm:ss");
});

Template.registerHelper("formatDateFromNow", function(date) {
	return moment(date).fromNow();
});

Template.registerHelper("getDurationInMinutes", function(startDate, endDate) {
	var startMoment = moment(startDate);
	var endMoment = moment(endDate);
	var minutes = endMoment.diff(startMoment, 'minutes');
	return minutes;
});

Template.registerHelper("isToday", function(date) {
	return moment(date).isSame(new Date(), 'day');
});
