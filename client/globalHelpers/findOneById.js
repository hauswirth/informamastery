// Collection access by primary key

Template.registerHelper("getUser", function(userId) {
	return Meteor.users.findOne({_id: userId});
});

Template.registerHelper("getCourse", function(courseId) {
	return Courses.findOne({_id: courseId});
});

Template.registerHelper("getTheme", function(themeId) {
	return Themes.findOne({_id: themeId});
});

Template.registerHelper("getTopic", function(topicId) {
	return Topics.findOne({_id: topicId});
});

Template.registerHelper("getSkill", function(skillId) {
	return Skills.findOne({_id: skillId});
});

Template.registerHelper("getStudyTask", function(studyTaskId) {
	return StudyTasks.findOne({_id: studyTaskId});
});

Template.registerHelper("getLab", function(labId) {
	return Labs.findOne({_id: labId});
});

Template.registerHelper("getContentItem", function(itemId) {
	return ContentItems.findOne({_id: itemId});
});

Template.registerHelper("getClassSession", function(classSessionId) {
	return ClassSessions.findOne({_id: classSessionId});
});

Template.registerHelper("getBook", function(bookId) {
	return Books.findOne({_id: bookId});
});

Template.registerHelper("getChapter", function(chapterId) {
	return Chapters.findOne({_id: chapterId});
});

Template.registerHelper("getSection", function(sectionId) {
	return Sections.findOne({_id: sectionId});
});

Template.registerHelper("getStudentRegistration", function(studentRegistrationId) {
	console.log("getStudentRegistration("+studentRegistrationId+")");
	return StudentRegistrations.findOne({_id: studentRegistrationId});
});

Template.registerHelper("getMasteryCheck", function(masteryCheckId) {
	return MasteryChecks.findOne({_id: masteryCheckId});
});

Template.registerHelper("getPracticeProblem", function(practiceProblemId) {
	return PracticeProblems.findOne({_id: practiceProblemId});
});

Template.registerHelper("getPracticeProblemSolution", function(practiceProblemSolutionId) {
	return PracticeProblemSolutions.findOne({_id: practiceProblemSolutionId});
});

Template.registerHelper("getAnnotation", function(annotationId) {
	return Annotations.findOne({_id: annotationId});
});

Template.registerHelper("getAnnotationResponse", function(annotationResponseId) {
	return AnnotationResponses.findOne({_id: annotationResponseId});
});

Template.registerHelper("getPomodoro", function(pomodoroId) {
	return Pomodoros.findOne({_id: pomodoroId});
});

Template.registerHelper("getBook", function(bookId) {
	return Books.findOne({_id: bookId});
});

Template.registerHelper("getChapter", function(chapterId) {
	return Chapters.findOne({_id: chapterId});
});

Template.registerHelper("getSection", function(sectionId) {
	return Sections.findOne({_id: sectionId});
});

Template.registerHelper("getSectionStatus", function(sectionStatusId) {
	console.log("getSectionStatus("+sectionStatusId+")")
	return SectionStatus.findOne({_id: sectionStatusId});
});

Template.registerHelper("getContentItemSubmission", function(contentItemSubmissionId) {
	console.log("getContentItemSubmission("+contentItemSubmissionId+")")
	return ContentItemSubmissions.findOne({_id: contentItemSubmissionId});
});

Template.registerHelper("getLabSubmissionFile", function(labSubmissionFileId) {
	console.log("getLabSubmissionFile("+labSubmissionFileId+")")
	// LabSubmissionFiles uses different kinds of IDs (by resumable),
	// so need to wrap ID in Mongo.ObjectID().
	// (But I don't fully understand this.)
	return LabSubmissionFiles.findOne({_id: new Mongo.ObjectID(labSubmissionFileId)});
});

Template.registerHelper("getLabFeedback", function(labFeedbackId) {
	console.log("getLabFeedback("+labFeedbackId+")")
	return LabFeedbacks.findOne({_id: labFeedbackId});
});

Template.registerHelper("getQuiz", function(quizId) {
	return Quizzes.findOne({_id: quizId});
});
