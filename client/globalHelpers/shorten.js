Template.registerHelper("shorten", function(name, w) {
  if (name && name.length) {
    if (!w) {
      w = 16;
    }
    w += w % 4;
    w = (w-4)/2
    if (name.length > 2*w) {
      return name.substring(0, w) + '…' + name.substring(name.length-w, name.length);
    } else {
      return name;
    }
  } else {
    return "(no name)";
  }
});
