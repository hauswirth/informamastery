// Note:
// When designing the Informa question/answer feature,
// don't fall into the same trap as StackOverflow:
// http://michael.richter.name/blogs/why-i-no-longer-contribute-to-stackoverflow

Template.questionPage.created = function () {
	console.log("Template.questionPage.created()");
	// ReactiveVar to hold the editing state
	this.isEditing = new ReactiveVar(false);
	console.log(this);
	var question = this.data.question;
	console.log(question);
	if (question) {
		var text = question.text;
		console.log(text);
		if (text) {
			this.isEditing.set(false);
		} else {
			this.isEditing.set(true);
		}
	} else {
		this.isEditing.set(true);
	}
};


Template.questionPage.rendered = function(){
	console.log("Template.questionPage.rendered()");
	this.$('.initially-focused').focus();
	//console.log(this);
};


Template.questionPage.helpers({
	isEditing: function() {
		// retrieve the isEditing state from the ReactiveVar set below
		return Template.instance().isEditing.get();
	},
	containerIs: function(itemId, collectionName) {
		return ContentItems.find({_id: itemId, containerCollection: collectionName}).count()>0;
	},
	countAnswers: function(annotationId) {
		return AnnotationResponses.find({annotationId: annotationId, removed: {$ne: true}}).count();
	},
	getAnswers: function(annotationId) {
		return AnnotationResponses.find({annotationId: annotationId, removed: {$ne: true}}, {sort: {score: -1}});
	},
	haveAnswered: function(questionId) {
		console.log("Template.questionPage.helpers.haveAnswered("+questionId+")");
		return AnnotationResponses.find({annotationId: questionId, removed: {$ne: true}, userId: Meteor.userId()}).count()>0;
	},
	haveUpvoted: function(question) {
		return question && question.upvoteUserIds && _.contains(question.upvoteUserIds, Meteor.userId());
	},
	haveDownvoted: function(question) {
		return question && question.downvoteUserIds && _.contains(question.downvoteUserIds, Meteor.userId());
	},
	haveNotVoted: function(question) {
		return question && ( !question.upvoteUserIds || !_.contains(question.upvoteUserIds, Meteor.userId()) )
		&& ( !question.downvoteUserIds || !_.contains(question.downvoteUserIds, Meteor.userId()) );
	},
});


Template.questionPage.events({
	'submit .new-answer-form': function(event, template) {
		console.log("Template.questionPage.events submit .new-answer-form");
		event.preventDefault();
		var annotationId = this.question._id;
		var value = event.target.answer.value;
		Meteor.call("submitNewAnswer", annotationId, value, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				console.log("saved.");
			}
		});
	},
	'click .upvote-question-button': function(event, template) {
		console.log("Template.questionPage.events click .upvote-question-button");
		var annotationId = this.question._id;
		Meteor.call("upvoteQuestion", annotationId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				console.log("done.");
			}
		});
	},
	'click .downvote-question-button': function(event, template) {
		console.log("Template.questionPage.events click .downvote-question-button");
		var annotationId = this.question._id;
		Meteor.call("downvoteQuestion", annotationId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				console.log("done.");
			}
		});
	},
	'click .clearvote-question-button': function(event, template) {
		console.log("Template.questionPage.events click .clearvote-question-button");
		var annotationId = this.question._id;
		Meteor.call("clearvoteQuestion", annotationId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				console.log("done.");
			}
		});
	},
	'click .remove-question-button': function(event, template) {
		console.log("Template.questionPage.events click .remove-question-button");
		var annotationId = this.question._id;
		Meteor.call("removeAnnotation", annotationId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				// redirect to my questions?
				console.log("done.");
			}
		});
	},
	'click .edit-question-button': function(event, template) {
		console.log("click .edit-question-button");
		var isEditing = template.isEditing.get();
		if (isEditing) {
			// save modified question text
			var annotationId = this.question._id;
	    console.log(annotationId);
			var text = $('.question-text-field').val();
	    console.log(text);
			Meteor.call("updateAnnotationText", annotationId, text, function(error, result) {
				if (error) {
	        console.log("Error");
					throwError(error.reason);
				} else {
					console.log("removed.");
				}
			})
			//TODO
		}
		// store the isEditing state in the ReactiveVar retrieved above
		template.isEditing.set(!isEditing);
	},
});
