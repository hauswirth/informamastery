Template.annotationButton.rendered = function() {
	this.$('.annotation-button-dropdown').dropdown();
};


Template.annotationButton.events({
	'click .add-annotation-item': function(event, template) {
		console.log("click .add-annotation-item");
		var contentItemId = this.contentItemId;
		console.log("contentItemId:", contentItemId);
		var skillId = this.skillId;
		console.log("skillId:", skillId);
		var sectionId = this.sectionId;
		console.log("sectionId:", sectionId);

		console.log("event:", event);

		var target = $(event.target);
		console.log("target:", target);

		var kind = target.attr("data-kind");
		console.log("kind:", kind);

		var id;
		var methodName;
		if (contentItemId) {
			id = contentItemId;
			methodName = "createContentItemAnnotation";
		} else if (sectionId) {
			id = sectionId;
			methodName = "createSectionAnnotation";
		} else {
			id = skillId;
			methodName = "createSkillAnnotation";
		}
		Meteor.call(methodName, id, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				var annotationId = result;
				Meteor.call("updateAnnotationKind", annotationId, kind, function(error, result) {
					if (error) {
						throwError(error.reason);
					} else {
						if (kind=="question") {
							console.log("Created a question, routing to questionPage");
							var annotation = Annotations.findOne({_id: annotationId});
							Router.go("questionPage", {courseId: annotation.courseId, questionId: annotationId});
						}
					}
				});
			}
		});
	},
});
