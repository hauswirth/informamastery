Template.studentQuestionListPage.helpers({
	countUserQuestionsForCourse: function(userId, courseId) {
		return Annotations.find({courseId: courseId, userId: userId, kind: "question", removed: false}).count();
	},
	getUserQuestionsForCourse: function(userId, courseId) {
		return Annotations.find({courseId: courseId, userId: userId, kind: "question", removed: false}, {sort: {modificationDate: -1}});
	},
	containerIs: function(itemId, collectionName) {
		return ContentItems.find({_id: itemId, containerCollection: collectionName}).count()>0;
	},
	makeSettings: function() {
		return {
			rowsPerPage: 50,
			class: "ui compact table",
			fields: [
				{
					key: "creationDate",
					label: "Created",
					sort: "descending",
					tmpl: Template.annotationCreationDateCell,
					headerClass: "collapsing",
					cellClass: "left aligned",
				},
				{
					key: "modificationDate",
					label: "Modified",
					sort: "descending",
					tmpl: Template.annotationModificationDateCell,
					headerClass: "collapsing",
					cellClass: "left aligned",
				},
				{
					key: "topicOrLab",
					label: "Topic / Lab",
					tmpl: Template.annotationTopicOrLabCell,
				},
				{
					key: "text",
					label: "Question",
					tmpl: Template.annotationTextCell,
				},
				{
					key: "score",
					label: "Score",
					headerClass: "collapsing",
					cellClass: "right aligned",
				},
				{
					key: "answerCount",
					label: "Answers",
					headerClass: "collapsing",
					cellClass: "right aligned",
					fn: function(value, object) {
						if (object) {
							return AnnotationResponses.find({annotationId: object._id}).count();
						} else {
							return 0;
						}
					},
				},
			],
		};
	},
});
