Template.studentAnnotationMenu.helpers({
	countStudentQuestions: function(studentId) {
		return Annotations.find({userId: studentId, kind: "question", removed: false}).count();
	},
	countStudentNotes: function(studentId) {
		return Annotations.find({userId: studentId, kind: "note", removed: false}).count();
	},
	countStudentTags: function(studentId) {
		return Annotations.find({userId: studentId, kind: "tag", removed: false}).count();
	},
	countStudentBookmarks: function(studentId) {
		return Annotations.find({userId: studentId, kind: "bookmark", removed: false}).count();
	},
});
