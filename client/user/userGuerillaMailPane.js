Template.userGuerillaMailPane.events({
	'submit .guerillamail-form': function(event, template) {
		console.log("guerillamail-form submitted");
		var studentRegistration = Template.currentData();
		var courseId = studentRegistration.courseId;
		var studentId = studentRegistration.studentId;
		var guerillamailCleartextAddress = event.target.guerillamailCleartextAddress.value;
		console.log("guerillamailCleartextAddress:", guerillamailCleartextAddress);
		var guerillamailScrambledAddress = event.target.guerillamailScrambledAddress.value;
		console.log("guerillamailScrambledAddress:", guerillamailScrambledAddress);
		Meteor.call('updateGuerillamailAddress', studentId, courseId, guerillamailCleartextAddress, guerillamailScrambledAddress, function(error, studentRegistrationId) {
			if (error) {
				throwError(error.reason);
			}
		});
		// Prevent default form submit
		return false;
	},
});
