Template.userGravatar.helpers({
	gravatarUrl: function(user) {
		return Gravatar.imageUrl(user.emails[0].address, {size: 50, secure: window.location.protocol==="https:"});
	},
});
