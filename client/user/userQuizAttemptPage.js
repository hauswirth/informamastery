Template.userQuizAttemptPage.helpers({
  getStudentRegistrationForCourseIdAndUserId: function(courseId, userId) {
    return StudentRegistrations.findOne({courseId: courseId, studentId: userId});
  },
  getChoiceForPracticeProblemIdAndLabel: function(quizAttempt, practiceProblemId, label) {
    console.log("Template.userQuizAttemptPage.helpers:getChoiceForPracticeProblemIdAndLabel(", quizAttempt, practiceProblemId, label, ")");
    var solutions = quizAttempt.solutions;
    var solution = _.find(solutions, function(solution) {return solution.practiceProblemId==practiceProblemId});
    console.log("solution:", solution);
    var choice = solution.choices[label];
    console.log("choice:", choice);
    return choice;
  },
  isQuizAttemptItemCorrect: function(quizAttempt, practiceProblem) {
    console.log("Template.userQuizAttemptPage.helpers:isQuizAttemptItemCorrect(", quizAttempt, practiceProblem, ")");
    return isQuizAttemptItemCorrect(quizAttempt, practiceProblem);
  },
  countCorrectQuizAttemptItems: function(quizAttempt) {
    console.log("Template.userQuizAttemptPage.helpers:countCorrectQuizAttemptItems(", quizAttempt, ")");
    return countCorrectQuizAttemptItems(quizAttempt);
  },
});
