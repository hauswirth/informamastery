Template.userStudyTaskItem.helpers({
	studyTaskCompleted: function(studyTaskId, userId) {
    console.log("studyTaskCompleted("+studyTaskId+", "+userId+")");
		var studyTaskStatus = StudyTaskStatus.findOne({studyTaskId: studyTaskId, userId: userId});
		if (studyTaskStatus && studyTaskStatus.recallText) {
			return true;
		} else {
			return false;
		}
	},
	getSumOfDurationsOfRemainingSections: function(sectionIds) {
		console.log("getSumOfDurationsOfRemainingSections("+sectionIds+")");
		return sectionIds?getSumOfDurationsOfRemainingSections(sectionIds):undefined;
	},
	bookStudyTaskCompleted: function(studyTaskId, userId) {
		console.log("bookStudyTaskCompleted("+studyTaskId+", "+userId+")");
		var studyTask = StudyTasks.findOne({_id: studyTaskId});
		if (studyTask && studyTask.detail) {
			var sectionIds = studyTask.detail.sectionIds;
			console.log("sectionIds: ", sectionIds);
			var allRecalled = true;
			_.each(sectionIds, function(sectionId) {
				var sectionStatus = SectionStatus.findOne({sectionId: sectionId, userId: userId});
				if (sectionStatus && sectionStatus.recallText) {
					// recalled
				} else {
					// not yet recalled
					allRecalled = false;
				}
			});
			return allRecalled;
		} else {
			return false;
		}
	},
	countStudyTaskSections: function(studyTaskId) {
		console.log("countStudyTaskSections("+studyTaskId+")");
		var studyTask = StudyTasks.findOne({_id: studyTaskId});
		if (studyTask && studyTask.detail) {
			return studyTask.detail.sectionIds.length;
		} else {
			return 0;
		}
	},
	countUserRecalledStudyTaskSections: function(studyTaskId, userId) {
		console.log("countUserRecalledStudyTaskSections("+studyTaskId+", "+userId+")");
		var studyTask = StudyTasks.findOne({_id: studyTaskId});
		if (studyTask && studyTask.detail) {
			var sectionIds = studyTask.detail.sectionIds;
			var recalledCount = 0;
			SectionStatus.find({sectionId: {$in: sectionIds}, userId: userId}).forEach(function(sectionStatus) {
				if (sectionStatus && sectionStatus.recallText) {
					// recalled
					recalledCount++;
				}
			});
			return recalledCount;
		} else {
			return 0;
		}
	},
	countUserStudyTaskSectionStatuses: function(studyTaskId, userId) {
		console.log("countUserStudyTaskSectionStatuses("+studyTaskId+", "+userId+")");
		var studyTask = StudyTasks.findOne({_id: studyTaskId});
		if (studyTask && studyTask.detail) {
			var sectionIds = studyTask.detail.sectionIds;
			return SectionStatus.find({sectionId: {$in: sectionIds}, userId: userId}).count();
		} else {
			return 0;
		}
	},
});
