Template.userPomodoroListPage.helpers({
	getPomodoros: function(courseId, userId) {
		return Pomodoros.find({courseId: courseId, userId: userId});
	},
	countPomodoros: function(courseId, userId) {
		return Pomodoros.find({courseId: courseId, userId: userId}).count();
	},
	countCompletedPomodoros: function(courseId, userId) {
		return Pomodoros.find({courseId: courseId, userId: userId, completed: true}).count();
	},
	countAbortedPomodoros: function(courseId, userId) {
		return Pomodoros.find({courseId: courseId, userId: userId, aborted: true}).count();
	},
	makeSettings: function() {
		return {
			rowsPerPage: 50,
			class: "ui compact table",
			fields: [
				{
					key: "startTime",
					label: "Started",
					sort: "descending",
					tmpl: Template.pomodoroStartTime,
					headerClass: "collapsing",
					cellClass: "left aligned",
				},
				{
					key: "topicOrLab",
					label: "Topic / Lab",
					tmpl: Template.pomodoroTopicOrLabCell,
					fn: function(value, object) {
						var topic = Topics.findOne({_id: object.topicId});
						var lab = Labs.findOne({_id: object.labId});
						return (topic?topic.title:"")+"/"+(lab?lab.title:"")},
					cellClass: "left aligned",
				},
				{
					key: "goal",
					label: "Goal",
					cellClass: "left aligned",
				},
				{
					key: "goalAccomplished",
					label: "Accomplished",
					tmpl: Template.pomodoroGoalAccomplishedCell,
					fn: function(value, object) {
						if (object.goalAccomplished==="fully") {
							return 4;
						} else if (object.goalAccomplished==="partially") {
							return 3;
						} else if (object.goalAccomplished==="no") {
							return 2;
						} else {
							return 1;
						}
					},
					cellClass: "left aligned",
				},
				{
					key: "completed",
					label: "Completed",
					tmpl: Template.pomodoroCompletedCell,
					fn: function(value, object) {
						return object.ongoing?"ongoing":(object.completed?"completed":"aborted");
					},
					headerClass: "collapsing",
					cellClass: "right aligned",
				},
			],
		};
	},
});
