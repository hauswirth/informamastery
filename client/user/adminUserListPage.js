Template.adminUserListPage.helpers({
	findUsers: function() {
		return Meteor.users.find({});
	},
	settings: {
		class: "ui compact table",
		fields: [
			{
				key: "gravatar",
				label: "Photo",
				sortable: false,
				tmpl: Template.userGravatar,
				headerClass: "collapsing",
			},
			{
				key: "profile.firstName",
				label: "First",
				sort: "ascending",
				tmpl: Template.userFirstName,
				headerClass: "collapsing",
			},
			{
				key: "profile.lastName",
				label: "Last",
				tmpl: Template.userLastName,
				headerClass: "collapsing",
			},
			{
				key: "primaryEmail",
				label: "Primary Email",
				tmpl: Template.userPrimaryMail,
				fn: function(value, object) {return object.emails[0].address},
			},
		],
	},
});
