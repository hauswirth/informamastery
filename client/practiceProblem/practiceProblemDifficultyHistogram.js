_computeDifficultyHistogram = function(practiceProblemId) {
	console.log("_computeDifficultyHistogram("+practiceProblemId+")");
	var histogram = [
		{label: "easy", count: 0, percentage: 0},
		{label: "medium", count: 0, percentage: 0},
		{label: "hard", count: 0, percentage: 0},
	];
	var count = 0;
	PracticeProblemSolutions.find({practiceProblemId: practiceProblemId}).forEach(function(practiceProblemSolution) {
		if (practiceProblemSolution.difficulty) {
			console.log("practiceProblemSolution.difficulty="+practiceProblemSolution.difficulty);
			histogram[practiceProblemSolution.difficulty].count++;
			count++;
		}
	});
	if (count) {
		histogram = _.map(histogram, function(element) {
			var percentage = Math.floor(100*element.count/count);
			console.log("histogram: "+element.label+" "+element.count+" "+percentage);
			return {
				label: element.label,
				count: element.count,
				percentage: percentage,
			};
		});
	}
	return histogram.reverse();
};


Template.practiceProblemDifficultyHistogram.helpers({
	getDifficultyHistogram: function(practiceProblemId) {
		return _computeDifficultyHistogram(practiceProblemId);
	},
});
