Template.computeQuizScoreSummaryPane.helpers({
  getQuizScoreSummary: function(courseId) {
    console.log("getQuizScoreSummary(", courseId, ")");
    var quizScoreSummary = Summaries.findOne({courseId: courseId, kind: "quizScoreSummary"} , {sort: {date: -1}, limit: 1});
    console.log("quizScoreSummary:", quizScoreSummary);
    return quizScoreSummary;
  },
});

Template.computeQuizScoreSummaryPane.events({
	'click .recompute-summary-button': function(ev, template) {
		console.log("recompute-summary-button clicked");
		var course = Template.currentData().course;
    console.log("course:", course);
		var courseId = course._id;
    Meteor.call("computeQuizScoreSummary", courseId, function(error, result) {
      if (error) {
        throwError(error.reason);
      }
    });
	},
});
