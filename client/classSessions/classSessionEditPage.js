var pickadateSubmitFormat = "mmmm d, yyyy";
var momentDateParseFormat = "MMMM D, YYYY";
//var pickadateDisplayFormat = "dddd mmmm d, yyyy";
var pickadateDisplayFormat = "ddd mmm d, yyyy";

var pickatimeSubmitFormat = "HH:i";
var momentTimeParseFormat = "HH:mm";
var pickatimeDisplayFormat = "HH:i";

Template.classSessionEditPage.rendered = function() {
	var self = this;
	console.log("Template.classSessionEditPage.rendered", self);

  $('.datepicker').pickadate({
    formatSubmit: pickadateSubmitFormat,
    format: pickadateDisplayFormat,
    onSet: function(thingSet) {
      //console.log('Set stuff:', thingSet);
    },
  });
  $('.timepicker').pickatime({
		formatSubmit: pickatimeSubmitFormat,
		format: pickatimeDisplayFormat,
		interval: 5,
	});

	this.$('.add-topic-dropdown').dropdown({
		onChange: function(value, text, selectedItem) {
			console.log("changed. value="+value+", text="+text+", selectedItem="+selectedItem);
      console.log(self);
			var classSessionId = self.data.classSession._id;
			var topicId = value;
			//console.log("this:", this);
			Meteor.call('addClassSessionTopic', classSessionId, topicId, function(error, result) {
				$('.add-topic-dropdown').dropdown("restore defaults");
				// for some reason the above attaches the default class, making the text grey
				$('.add-topic-dropdown span.text.default').removeClass("default");
				if (error) {
					throwError(error.reason);
				}
			});
		},
	});
};


Template.classSessionEditPage.events({
	'submit .update-form': function(event, template) {
		var courseId = this.course._id;
		var classSessionId = this.classSession._id;
		var title = event.target.title.value;
		var description = event.target.description.value;

    var theDate = null;
    var dateMoment = moment(event.target.date_submit.value, momentDateParseFormat);
    console.log(dateMoment);
    var timeMoment = moment(event.target.time_submit.value, momentTimeParseFormat);
    console.log(timeMoment);

    if (dateMoment.isValid() && timeMoment.isValid()) {
	    var dateDate = dateMoment.toDate();
			console.log(dateDate);

	    var timeDate = timeMoment.toDate();
			console.log(timeDate);

			var theMoment = dateMoment;
			theMoment.minutes(timeMoment.minutes());
			theMoment.hours(timeMoment.hours());
			console.log(theMoment);
			theDate = theMoment.toDate();
		}
		console.log(theDate);

    var duration = event.target.duration.value;
    var preparation = event.target.preparation.value;
    var materials = event.target.materials.value;

		Meteor.call('updateClassSession', classSessionId, title, description, theDate, duration, preparation, materials, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("classSessionPage", {courseId: courseId, classSessionId: classSessionId});
			}
		});
		// Prevent default form submit
		return false;
	},
	'click .remove-topic-button': function(event, template) {
		event.preventDefault();
		console.log("click .remove-topic-button");
		var topicId = this._id;
		//console.log("this: ", this);
		//console.log("Template.instance(): ", Template.instance());
		//console.log("Template.currentData(): ", Template.currentData());
		//console.log("Template.parentData(): ", Template.parentData());
		//console.log("Template.parentData(2): ", Template.parentData(2));
		var classSessionId = Template.currentData().classSession._id;
		Meteor.call("removeClassSessionTopic", classSessionId, topicId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});


Template.classSessionEditPage.helpers({
	getTopics: function(topicIds) {
    if (!topicIds) {
      topicIds = [];
    }
		return Topics.find({_id: {$in: topicIds}});
	},
	getCourseTopics: function(courseId) {
		return Topics.find({courseId: courseId, removed: {$ne: true}});
	},
  getSkills: function(topicIds) {
    if (!topicIds) {
      topicIds = [];
    }
    return Skills.find({topicId: {$in: topicIds}}, {sort: {topicId: 1}});
  },
  getStudyTasks: function(topicIds) {
    if (!topicIds) {
      topicIds = [];
    }
		return StudyTasks.find({topicId: {$in: topicIds}}, {sort: {topicId: 1}});
	},
  makeSettings: function() {
    return {
      rowsPerPage: 50,
      class: "ui compact table",
      fields: [
        {
          key: "topicAndTitle",
          label: "Topic & Title",
          cellClass: "left aligned",
          tmpl: Template.studyTaskTopicAndTitle,
          fn: function(value, object) {return object.title;},
        },
        {
          key: "use",
          label: "Use",
          headerClass: "collapsing",
          cellClass: "right aligned",
          tmpl: Template.classSessionEditPageUseStudyTask,
        },
      ],
    };
  },
});
