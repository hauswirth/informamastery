Template.classSessionEditPageUseStudyTask.helpers({
  isStudyTaskInClassSession: function() {
    //console.log("isStudyTaskInClassSession()");
    var studyTask = Template.currentData();
    //console.log("studyTask: ", studyTask);
    // HACK:
    // We assume we sit inside a reactiveTable.
    // We assume our context is a StudyTask.
    // We have to skip over a whole bunch of reactive-table-specific parent contexts
    // to find the context surrounding the reactive table,
    // which is the context that contains the course and classSession keys,
    // the context of the classSessionEditPage template.
    /*
    console.log("context:", Template.currentData());
    console.log("parentContext(0)==context:", Template.parentData(0));
    console.log("parentContext(1):", Template.parentData(1));
    console.log("parentContext(2):", Template.parentData(2));
    console.log("parentContext(3):", Template.parentData(3));
    console.log("parentContext(4):", Template.parentData(4));
    console.log("parentContext(5):", Template.parentData(5));
    console.log("parentContext(6):", Template.parentData(6));
    */
    var classSession = Template.parentData(6).classSession;
    //console.log("classSession: ", classSession);
    return _.contains(classSession.studyTaskIds, studyTask._id);
  },
});


Template.classSessionEditPageUseStudyTask.events({
	'click .set-use-button': function(event, template) {
		event.preventDefault();
		console.log("click .set-use-button");
  	console.log("context:", Template.currentData());
    console.log("parentContext(0)==context:", Template.parentData(0));
    console.log("parentContext(1):", Template.parentData(1));
    console.log("parentContext(2):", Template.parentData(2));
    console.log("parentContext(3):", Template.parentData(3));
    console.log("parentContext(4):", Template.parentData(4));
    console.log("parentContext(5):", Template.parentData(5));
    console.log("parentContext(6):", Template.parentData(6));
    /*
    */
    // this seems to point to the study task (but template.currentData() doesn't!)
		var studyTask = this;
		console.log("studyTask: ", studyTask);
    //HACK: find classSession in ancestor context
    var classSession = Template.parentData(6).classSession;
    console.log("classSession: ", classSession);
		Meteor.call("addClassSessionStudyTask", classSession._id, studyTask._id, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .clear-use-button': function(event, template) {
		event.preventDefault();
		console.log("click .clear-use-button");
    /*
  	console.log("context:", Template.currentData());
    console.log("parentContext(0)==context:", Template.parentData(0));
    console.log("parentContext(1):", Template.parentData(1));
    console.log("parentContext(2):", Template.parentData(2));
    console.log("parentContext(3):", Template.parentData(3));
    console.log("parentContext(4):", Template.parentData(4));
    console.log("parentContext(5):", Template.parentData(5));
    console.log("parentContext(6):", Template.parentData(6));
    */
    // this seems to point to the study task (but template.currentData() doesn't!)
		var studyTask = this;
		console.log("studyTask: ", studyTask);
    //HACK: find classSession in ancestor context
    var classSession = Template.parentData(6).classSession;
    console.log("classSession: ", classSession);
		Meteor.call("removeClassSessionStudyTask", classSession._id, studyTask._id, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
