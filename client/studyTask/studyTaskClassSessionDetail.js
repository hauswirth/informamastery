Template.studyTaskClassSessionDetail.created = function () {
	console.log("Template.studyTaskClassSessionDetail.created()");
	var template = this;
	//console.log("template:", template);

	template.selectedClassSessionId = new ReactiveVar(null);
	// Populate selectedClassSessionId reactive var when the data context gets populated
	template.autorun(function(computation) {
		var context = Template.currentData(); // because currentData() is reactive, this reruns as needed!
		var classSessionId = null;
		if (context.studyTask && context.studyTask.detail && context.studyTask.detail.classSessionId) {
			classSessionId = context.studyTask.detail.classSessionId;
		} else {
			// select the first class session we find for this course
			if (context.studyTask && context.studyTask.courseId) {
				var classSession = ClassSessions.findOne({courseId: context.studyTask.courseId});
				if (classSession) {
					classSessionId = classSession._id;
				}
			}
		}
		console.log("@@ Context -> ReactiveVar, classSessionId=", classSessionId);
		template.selectedClassSessionId.set(classSessionId);
	});
};

Template.studyTaskClassSessionDetail.rendered = function() {
	console.log("Template.studyTaskClassSessionDetail.rendered()");
	var template = this;
	//console.log("template:", template);

	var classSessionDropdown = template.$('.class-session-dropdown');
	var firstClassSessionChange = true;
	classSessionDropdown.dropdown({
		onChange: function(value, text, selectedItem) {
			console.log("class-session-dropdown changed", "value:", value, "text:", text, "selectedItem:", selectedItem);
			if (firstClassSessionChange) {
				firstClassSessionChange = false;
			} else {
				var classSessionId = value;
				console.log("@@ Dropdown -> ReactiveVar, classSessionId=", classSessionId);
				template.selectedClassSessionId.set(classSessionId);
			}
		},
	});

	// reactively update classSessionDropdown whenever selectedClassSessionId reactive var changes
	template.autorun(function(computation) {
		var classSessionId = template.selectedClassSessionId.get();
		console.log("@@ ReactiveVar -> Dropdown, classSessionId=", classSessionId);
		console.log("classSessionDropdown:", classSessionDropdown);
		classSessionDropdown.dropdown("set selected", classSessionId);
	});
};


Template.studyTaskClassSessionDetail.helpers({
	getClassSessions: function(courseId) {
		console.log("Template.studyTaskClassSessionDetail.helpers.getClassSessions("+courseId+")");
		return ClassSessions.find({courseId: courseId});
	},
	updateClassSessionDropdown: function() {
		//console.log("Template.studyTaskClassSessionDetail.helpers.updateClassSessionDropdown()");
		//var template = Template.instance();
		//Meteor.defer(function() {
			//console.log("Template.studyTaskClassSessionDetail.helpers.updateClassSessionDropdown..deferredFunc");
			//console.log("template:", template);
		//});
	},
});
