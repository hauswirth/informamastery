Template.studyTaskRecallsPage.helpers({
	countStartedStatuses: function(studyTaskId) {
		//console.log("countStartedStatuses("+studyTaskId+")");
		return StudyTaskStatus.find({studyTaskId: studyTaskId, started: {$exists: true}}).count();
	},
	countFinishedStatuses: function(studyTaskId) {
		//console.log("countFinishedStatuses("+studyTaskId+")");
		return StudyTaskStatus.find({studyTaskId: studyTaskId, finished: {$exists: true}}).count();
	},
	countRecalledStatuses: function(studyTaskId) {
		//console.log("countRecalledStatuses("+studyTaskId+")");
		return StudyTaskStatus.find({studyTaskId: studyTaskId, recallText: {$exists: true}}).count();
	},
	getRecalledStatuses: function(studyTaskId) {
		//console.log("getRecalledStatuses("+studyTaskId+")");
		return StudyTaskStatus.find({studyTaskId: studyTaskId, recallText: {$exists: true}}, {sort: {modificationDate: -1}});
	},
	getMostRecentlyStartedStatus: function(studyTaskId) {
		//console.log("getMostRecentlyStartedStatus("+studyTaskId+")");
		return StudyTaskStatus.findOne({studyTaskId: studyTaskId, started: true}, {sort: {startedDate: -1}});
	},
});
