Template.studyTaskTextbookDetail.created = function () {
	console.log("Template.studyTaskTextbookDetail.created()");
	var template = this;
	//console.log("template:", template);

	template.selectedBookId = new ReactiveVar(null);
	// Populate selectedBookId reactive var when the data context gets populated
	template.autorun(function(computation) {
		var context = Template.currentData(); // because currentData() is reactive, this reruns as needed!
		var bookId = null;
		if (context.studyTask && context.studyTask.detail && context.studyTask.detail.bookId) {
			bookId = context.studyTask.detail.bookId;
		} else {
			// select the first book we find for this course
			if (context.studyTask && context.studyTask.courseId) {
				var book = Books.findOne({courseId: context.studyTask.courseId});
				if (book) {
					bookId = book._id;
				}
			}
		}
		console.log("@@ Context -> ReactiveVar, bookId=", bookId);
		template.selectedBookId.set(bookId);
	});

	template.selectedChapterId = new ReactiveVar(null);
	// Populate selectedChapterId reactive var when the data context gets populated
	template.autorun(function(computation) {
		var context = Template.currentData(); // because currentData() is reactive, this reruns as needed!
		var chapterId = null;
		if (context.studyTask && context.studyTask.detail && context.studyTask.detail.chapterId) {
			chapterId = context.studyTask.detail.chapterId;
		}
		console.log("@@ Context -> ReactiveVar, chapterId=", chapterId);
		template.selectedChapterId.set(chapterId);
	});
};

Template.studyTaskTextbookDetail.rendered = function() {
	console.log("Template.studyTaskTextbookDetail.rendered()");
	var template = this;
	//console.log("template:", template);

	var bookDropdown = template.$('.book-dropdown');
	var firstBookChange = true;
	bookDropdown.dropdown({
		onChange: function(value, text, selectedItem) {
			console.log("book-dropdown changed", "value:", value, "text:", text, "selectedItem:", selectedItem);
			if (firstBookChange) {
				firstBookChange = false;
			} else {
				var bookId = value;
				console.log("@@ Dropdown -> ReactiveVar, bookId=", bookId);
				template.selectedBookId.set(bookId);
			}
		},
	});

	// reactively update bookDropdown whenever selectedBookId reactive var changes
	template.autorun(function(computation) {
		var bookId = template.selectedBookId.get();
		console.log("@@ ReactiveVar -> Dropdown, bookId=", bookId);
		console.log("bookDropdown:", bookDropdown);
		bookDropdown.dropdown("set selected", bookId);
	});

	var chapterDropdown = template.$('.chapter-dropdown');
	var firstChapterChange = true;
	chapterDropdown.dropdown({
		onChange: function(value, text, selectedItem) {
			console.log("chapter-dropdown changed", "value:", value, "text:", text, "selectedItem:", selectedItem);
			if (firstChapterChange) {
				firstChapterChange = false;
			} else {
				var chapterId = value;
				console.log("@@ Dropdown -> ReactiveVar, chapterId=", chapterId);
				template.selectedChapterId.set(chapterId);
			}
		},
	});

	// reactively update chapterDropdown whenever selectedChapterId reactive var changes
	template.autorun(function(computation) {
		var chapterId = template.selectedChapterId.get();
		console.log("@@ ReactiveVar -> Dropdown, chapterId=", chapterId);
		console.log("chapterDropdown:", chapterDropdown);
		chapterDropdown.dropdown("set selected", chapterId);
	});
};


Template.studyTaskTextbookDetail.helpers({
	getBooks: function(courseId) {
		console.log("Template.studyTaskTextbookDetail.helpers.getBooks("+courseId+")");
		return Books.find({courseId: courseId});
	},
	updateBookDropdown: function() {
		//console.log("Template.studyTaskTextbookDetail.helpers.updateBookDropdown()");
		//var template = Template.instance();
		//Meteor.defer(function() {
			//console.log("Template.studyTaskTextbookDetail.helpers.updateBookDropdown..deferredFunc");
			//console.log("template:", template);
		//});
	},
	getChaptersOfSelectedBook: function() {
		console.log("Template.studyTaskTextbookDetail.helpers.getChaptersOfSelectedBook()");
		return Chapters.find({bookId: Template.instance().selectedBookId.get()});
	},
	updateChapterDropdown: function() {
		//console.log("Template.studyTaskTextbookDetail.helpers.updateChapterDropdown()");
		//var template = Template.instance();
		//Meteor.defer(function() {
			//console.log("Template.studyTaskTextbookDetail.helpers.updateChapterDropdown..deferredFunc");
			//console.log("template:", template);
		//});
	},
	getSectionsOfSelectedChapter: function() {
		console.log("Template.studyTaskTextbookDetail.helpers.getSectionsOfSelectedChapter()");
		return Sections.find({chapterId: Template.instance().selectedChapterId.get(), removed: false}, {sort: {sortKey: 1}});
	},
});
