Template.studyTaskEditPage.created = function () {
	console.log("Template.studyTaskEditPage.created()");
	var template = this;
	//console.log("template:", template);
	template.selectedKind = new ReactiveVar(null);
	// Populate selectedKind reactive var when the data context gets populated
	template.autorun(function(computation) {
		var data = Template.currentData(); // because currentData() is reactive, this reruns as needed!
		var kind = data.studyTask?data.studyTask.kind:null;
		console.log("$$$$$$ Context -> ReactiveVar, kind=", kind);
		template.selectedKind.set(kind);
	});
};

Template.studyTaskEditPage.rendered = function() {
	console.log("Template.studyTaskEditPage.rendered()");
	var template = this;
	//console.log("template:", template);

	var dropdownAsFoundWhenFirstRendered = template.$('.kind-dropdown');
	var firstChange = true;
	dropdownAsFoundWhenFirstRendered.dropdown({
		onChange: function(value, text, selectedItem) {
			console.log("kind-dropdown changed", "value:", value, "text:", text, "selectedItem:", selectedItem);
			if (firstChange) {
				firstChange = false;
			} else {
				var kind = value;
				console.log("$$$$$$ Dropdown -> ReactiveVar, kind=", kind);
				template.selectedKind.set(kind);
			}
		},
	});

	// reactively update dropdown whenever selectedKind reactive var changes
	template.autorun(function(computation) {
		var kind = template.selectedKind.get();
		console.log("$$$$$$ ReactiveVar -> Dropdown, kind=", kind);
		console.log("dropdownAsFoundWhenFirstRendered:", dropdownAsFoundWhenFirstRendered);
		dropdownAsFoundWhenFirstRendered.dropdown("set selected", kind);
	});
};


Template.studyTaskEditPage.helpers({
	updateKindDropdown: function(kind) {
		console.log("Template.studyTaskEditPage.helpers.updateKindDropdown()");
		// NOTE: This is copied and adapted from progressbar code (see topics.helpers.js),
		//       this may be overkill, or it may not work properly (it's untested).
		//
		// Getting this dropdown to update reactively was really, REALLY painful!
		// Template.studyTaskEditPage.rendered() is NOT called when the template changes reactively.
		// So to call some jQuery code ($('select.dropdown').dropdown()) to get SemanticUI's dropdown
		// to update itself reactively has to be done otherwise.
		// After a LOT of StackOverflow and Google, I found out that we can use a helper (this one)
		// that will be triggered reactively (here, because the dummy = kind computation depends on changed values).
		// Then, given that the helper is run when the data changes (specifically, when the contents of the template is recomputed),
		// we need to somehow update the DOM. To access the DOM, we can get the Template.instance().
		// But at the time this helper runs, the DOM does not yet exist.
		// So we defer(), and only retrieve the dropdown's DOM node then (after the template has been rendered/updated).
		//console.log("Template.studyTaskEditPage.helpers.updateDropdown("+kind+")");
		var dummy = kind;
		var template = Template.instance();
		Meteor.defer(function() {
			console.log("Template.studyTaskEditPage.helpers.updateKindDropdown..deferredFunc");
			//console.log("template:", template);
		});
	},
	selectedIfSame: function(kind1, kind2) {
		//console.log("Template.studyTaskEditPage.helpers.selectedIfSame("+kind1+", "+kind2+")");
		return kind1===kind2?"selected":"";
	},
	selectedKindIs: function(value) {
		console.log("Template.studyTaskEditPage.helpers.selectedKindIs("+value+")");
		return Template.instance().selectedKind.get()==value;
	},
});


Template.studyTaskEditPage.events({
	'submit .form': function (event, template) {
		console.log("Template.studyTaskEditPage.events: submit .form");
		var studyTaskId = this.studyTask._id;
		var courseId = this.course._id;
		var title = event.target.title.value;
		var description = event.target.description.value;
		var duration = event.target.duration.value;
		var kind = event.target.kind.value;
		//console.log("Template.studyTaskEditPage.events--submit .form:");
		//console.log(event.target);
		//console.log(event.target.kind);
		var body = event.target.body.value;

		var detail = {};
		if (kind==="ClassSession") {
			detail.classSessionId = $(".class-session-dropdown").dropdown("get value");
			console.log("classSessionId:", detail.classSessionId);
		}
		if (kind==="Textbook") {
			detail.bookId = $(".book-dropdown").dropdown("get value");
			console.log("bookId:", detail.bookId);

			detail.chapterId = $(".chapter-dropdown").dropdown("get value");
			console.log("chapterId:", detail.chapterId);

			//console.log("$(...):", $(".section-checkbox.checked"));
			detail.sectionIds = $(".section-checkbox.checked").map(function() {
				return $(this).attr('data-sectionId');
			}).get();
			console.log("sectionIds:", detail.sectionIds);
		}

		Meteor.call('updateStudyTask', studyTaskId, title, description, kind, duration, body, detail, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("studyTaskPage", {courseId: courseId, studyTaskId: studyTaskId});
			}
		});
		// Prevent default form submit
		return false;
	},
});
