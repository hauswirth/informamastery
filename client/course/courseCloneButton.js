Template.courseCloneButton.events({
	'click .clone-course-button': function (event) {
		var courseId = this._id;
		Meteor.call('cloneCourse', courseId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
		// Prevent default form submit
		return false;
	},
});
