Template.studentPage.helpers({
	getMasteryChecksForStudentInCourse: function(studentId, courseId) {
		return MasteryChecks.find({studentId: studentId, courseId: courseId}, {sort: {startDate: -1}});
	},
	getTopicsOfStudentForCourseToBeChecked: function(studentId, courseId) {
		return getTopicsOfStudentForCourseToBeChecked(studentId, courseId);
	},
	countOwnPracticeProblems: function(studentId, courseId) {
		return PracticeProblems.find({userId: studentId, courseId: courseId, published: true, archived: false}).count();
	},
	countSolutionsOfOwnPracticeProblems: function(studentId, courseId) {
		var myPracticeProblemIds = PracticeProblems.find({userId: studentId, courseId: courseId, published: true, archived: false}).map(function(problem) {return problem._id;});
		return PracticeProblemSolutions.find({practiceProblemId: {$in: myPracticeProblemIds}}).count();
	},
	determineAverageOwnPracticeProblemQuality: function(studentId, courseId) {
		var myPracticeProblemIds = PracticeProblems.find({userId: studentId, courseId: courseId, published: true, archived: false}).map(function(problem) {return problem._id;});
		var average = _computeAverageQualityRatingAcrossProblems(myPracticeProblemIds);
		return _convertQualityNumberToText(average)
			+ (average>=0 ? (" ("+(Math.round(average*10)/10)+")") : "unknown");
	},
	determineAverageOwnPracticeProblemDifficulty: function(studentId, courseId) {
		var myPracticeProblemIds = PracticeProblems.find({userId: studentId, courseId: courseId, published: true, archived: false}).map(function(problem) {return problem._id;});
		var average = _computeAverageDifficultyRatingAcrossProblems(myPracticeProblemIds);
		return _convertDifficultyNumberToText(average)
			+ (average>=0 ? (" ("+(Math.round(average*10)/10)+")") : "unknown");
	},
	getMostRecentPracticeProblemSolutionWithComment: function(studentId, courseId) {
		var myPracticeProblemIds = PracticeProblems.find({userId: studentId, courseId: courseId, published: true, archived: false}).map(function(problem) {return problem._id;});
		return PracticeProblemSolutions.find({practiceProblemId: {$in: myPracticeProblemIds}, comment: {$exists: true, $ne: ""}}, {sort: {feedbackDate: -1}, limit: 1});
	},
	getArrayLength: function(array) {
		return array?array.length:"?";
	},
	countStudentCourseSectionStatuses: function(userId, courseId) {
    console.log("countStudentCourseSectionStatuses("+userId+", "+courseId+")");
    return SectionStatus.find({courseId: courseId, userId: userId}).count();
  },
	getMostRecentSectionStatus: function(userId, courseId) {
		return SectionStatus.find({courseId: courseId, userId: userId}, {sort: {modificationDate: -1}, limit: 1});
	},
	getPhoto: function(studentRegistrationId) {
		console.log("Template.studentPage.helpers:getPhoto("+studentRegistrationId+")");
		return StudentPhotoFiles.findOne({"metadata.studentRegistrationId": studentRegistrationId});
	},
	getPhotoLink: function() {
    return StudentPhotoFiles.baseURL + "/md5/" + this.md5;
  },
	gravatarUrl: function(user) {
		return Gravatar.imageUrl(user.emails[0].address, {size: 200, secure: window.location.protocol==="https:"});
	},
});


Template.studentPage.events({
	'click .approve-user-as-student-button': function(ev, template) {
		console.log("approve-user-as-student-button clicked");
		var studentRegistration = Template.currentData();
		var courseId = studentRegistration.courseId;
		var studentId = studentRegistration.studentId;
		Meteor.call('approveUserAsStudent', studentId, courseId, function(error, studentRegistrationId) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .start-mastery-check-button': function(ev, template) {
		console.log("start-mastery-check-button clicked");
		var studentRegistration = Template.currentData();
		var courseId = studentRegistration.courseId;
		var studentId = studentRegistration.studentId;
		Meteor.call('startMasteryCheck', studentId, courseId, function(error, masteryCheckId) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("/masteryCheck/"+masteryCheckId);
			}
		});
	},
	'click .withdraw-button': function(ev, template) {
		console.log("withdraw-button clicked");
		var studentRegistration = Template.currentData();
		var courseId = studentRegistration.courseId;
		var studentId = studentRegistration.studentId;
		Meteor.call('withdrawUserAsStudent', studentId, courseId, function(error, studentRegistrationId) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .undo-withdrawal-button': function(ev, template) {
		console.log("undo-withdrawal-button clicked");
		var studentRegistration = Template.currentData();
		var courseId = studentRegistration.courseId;
		var studentId = studentRegistration.studentId;
		Meteor.call('undoWithdrawalOfUserAsStudent', studentId, courseId, function(error, studentRegistrationId) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'submit .guerillamail-form': function(event, template) {
		console.log("guerillamail-form submitted");
		var studentRegistration = Template.currentData();
		var courseId = studentRegistration.courseId;
		var studentId = studentRegistration.studentId;
		var guerillamailCleartextAddress = event.target.guerillamailCleartextAddress.value;
		console.log("guerillamailCleartextAddress:", guerillamailCleartextAddress);
		var guerillamailScrambledAddress = event.target.guerillamailScrambledAddress.value;
		console.log("guerillamailScrambledAddress:", guerillamailScrambledAddress);
		Meteor.call('updateGuerillamailAddress', studentId, courseId, guerillamailCleartextAddress, guerillamailScrambledAddress, function(error, studentRegistrationId) {
			if (error) {
				throwError(error.reason);
			}
		});
		// Prevent default form submit
		return false;
	},
});
