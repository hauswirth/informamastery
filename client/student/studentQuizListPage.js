function _getPastQuizzes(courseId) {
  // quizzes which have a stopDate that's earlier than now
  var now = new Date();
  //var start = moment().startOf('day').toDate();
  return Quizzes.find({courseId: courseId, removed: false, stopDate: {$lt: now}}, {sort: {date: 1}});
}

Template.studentQuizListPage.helpers({
  getPastQuizzes: function(courseId) {
    return _getPastQuizzes(courseId);
  },
  countPastQuizzes: function(courseId) {
    return _getPastQuizzes(courseId).count();
  },
  countPastQuizzesTaken: function(courseId, studentId) {
    var pastQuizzes = _getPastQuizzes(courseId);
    var quizzesTaken = 0;
    pastQuizzes.forEach(function(quiz) {
      if (QuizAttempts.find({quizId: quiz._id, studentId: studentId}).count()>0) {
        quizzesTaken++;
      }
    });
    return quizzesTaken;
  },
  getQuizAttempt: function(quizId, studentId) {
    // the LAST attempt of that quiz!
    var quiz = Quizzes.findOne({_id: quizId});
    return QuizAttempts.findOne({quizId: quizId, studentId: studentId, quizStartDate: quiz.startDate});
  },
});
