function _getOngoingClassSession(courseId) {
  console.log("_getOngoingClassSession("+courseId+")");
	var now = new Date();
  console.log("now:", now);
  var latestSessionThatStarted = ClassSessions.findOne({courseId: courseId, removed: false, date: {$lt: now}}, {sort: {date: 1}, limit: 1});
  console.log("latestSessionThatStarted:", latestSessionThatStarted);
  if (latestSessionThatStarted) {
    var duration = latestSessionThatStarted.duration;
    console.log("duration:", duration);
    var nowMoment = moment(now);
    console.log("nowMoment:", nowMoment);
    var startMoment = moment(latestSessionThatStarted.date);
    console.log("startMoment:", startMoment);
    var endMoment = moment(startMoment);
    endMoment.add(latestSessionThatStarted.duration, "minutes");
    console.log("endMoment:", endMoment);
    if (nowMoment.isBefore(endMoment)) {
      return latestSessionThatStarted;
    }
  }
  return null;
}

function _getNextClassSession(courseId) {
	var now = new Date();
  return ClassSessions.findOne({courseId: courseId, removed: false, date: {$gt: now}}, {sort: {date: 1}, limit: 1});
}

Template.studentPageClassSessionPane.helpers({
  getOngoingClassSession: function(courseId) {
    console.log("getOngoingClassSession("+courseId+")");
    return _getOngoingClassSession(courseId);
	},
  getNextClassSession: function(courseId) {
		return _getNextClassSession(courseId);
	},
});
