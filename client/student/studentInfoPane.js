Template.studentInfoPane.helpers({
  getPhoto: function(studentRegistrationId) {
		console.log("Template.studentInfoPane.helpers:getPhoto("+studentRegistrationId+")");
		return StudentPhotoFiles.findOne({"metadata.studentRegistrationId": studentRegistrationId});
	},
	getPhotoLink: function() {
    return StudentPhotoFiles.baseURL + "/md5/" + this.md5;
  },
	gravatarUrl: function(user) {
		return Gravatar.imageUrl(user.emails[0].address, {size: 200, secure: window.location.protocol==="https:"});
	},
});
