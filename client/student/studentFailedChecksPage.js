Template.studentFailedChecksPage.helpers({
	getFailedTopicChecksForCourseAndStudent: function(courseId, studentId) {
		return TopicChecks.find({courseId: courseId, $or: [{studentId: studentId}, {studentIds: studentId}], mastered: false}, {sort: {endDate: -1}});
	},
	getResolvedBugsForCourseAndStudent: function(courseId, studentId) {
		var masteredTopicIds = TopicChecks.find({courseId: courseId, $or: [{studentId: studentId}, {studentIds: studentId}], mastered: true}).map(function(topicCheck) {return topicCheck.topicId;});
		return TopicChecks.find({courseId: courseId, $or: [{studentId: studentId}, {studentIds: studentId}], topicId: {$in: masteredTopicIds}, mastered: false}, {sort: {endDate: -1}});
	},
	getPendingBugsForCourseAndStudent: function(courseId, studentId) {
		var masteredTopicIds = TopicChecks.find({courseId: courseId, $or: [{studentId: studentId}, {studentIds: studentId}], mastered: true}).map(function(topicCheck) {return topicCheck.topicId;});
		return TopicChecks.find({courseId: courseId, $or: [{studentId: studentId}, {studentIds: studentId}], topicId: {$nin: masteredTopicIds}, mastered: false}, {sort: {endDate: -1}});
	},
	countPendingBugsForCourseAndStudent: function(courseId, studentId) {
		var masteredTopicIds = TopicChecks.find({courseId: courseId, $or: [{studentId: studentId}, {studentIds: studentId}], mastered: true}).map(function(topicCheck) {return topicCheck.topicId;});
		return TopicChecks.find({courseId: courseId, $or: [{studentId: studentId}, {studentIds: studentId}], topicId: {$nin: masteredTopicIds}, mastered: false}, {sort: {endDate: -1}}).count();
	},
	hasMultiplePendingBugsForCourseAndStudent: function(courseId, studentId) {
		var masteredTopicIds = TopicChecks.find({courseId: courseId, $or: [{studentId: studentId}, {studentIds: studentId}], mastered: true}).map(function(topicCheck) {return topicCheck.topicId;});
		return TopicChecks.find({courseId: courseId, $or: [{studentId: studentId}, {studentIds: studentId}], topicId: {$nin: masteredTopicIds}, mastered: false}, {sort: {endDate: -1}}).count()>1;
	},
});
