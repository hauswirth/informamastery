Template.studentPassedChecksPage.helpers({
	getMasteredTopicChecksForCourseAndStudent: function(courseId, studentId) {
		return TopicChecks.find({courseId: courseId, $or: [{studentIds: studentId}, {studentId: studentId}], mastered: true}, {sort: {endDate: -1}});
	},
	hasMasteredTopicChecksForCourseAndStudent: function(courseId, studentId) {
		return TopicChecks.find({courseId: courseId, $or: [{studentIds: studentId}, {studentId: studentId}], mastered: true}).count()>0;
	},
});
