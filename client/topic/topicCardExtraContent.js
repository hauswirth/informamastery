Template.topicCardExtraContent.helpers({
	isTopicMastered: isTopicMastered,
	countAllStudentsPassedTopicChecksForTopic: function(topicId) {
		return TopicChecks.find({topicId: topicId, mastered: true}).count();
	},
	countAllStudentsFailedTopicChecksForTopic: function(topicId) {
		return TopicChecks.find({topicId: topicId, mastered: false}).count();
	},
	countAllQuestionsForTopic: function(topicId) {
		var skillIds = Skills.find({topicId: topicId}).map(function(skill) {
			return skill._id;
		});
		return Annotations.find({skillId: {$in: skillIds}, removed: {$ne: true}, kind: "question"}).count();
	},
	countAllPublishedUnarchivedPracticeProblemsForTopic: function(topicId) {
		return PracticeProblems.find({topicId: topicId, published: true, archived: false}).count();
	},
	updateProgressBar: function(topicId) {
		// Getting this progress bar to update reactively was really, REALLY painful!
		// Template.showTopic.rendered() is NOT called when the template changes reactively.
		// So to call some jQuery code ($('#progress').progress()) to get SemanticUI's progress bar
		// to update itself reactively has to be done otherwise.
		// After a LOT of StackOverflow and Google, I found out that we can use a helper (this one)
		// that will be triggered reactively (here, because the computeProgress() computation depends on changed values).
		// Then, given that the helper is run when the data changes (specifically, when the contents of the template is recomputed),
		// we need to somehow update the DOM. To access the DOM, we can get the Template.instance().
		// But at the time this helper runs, the DOM does not yet exist.
		// So we defer(), and only retrieve the progress bar's DOM node then (after the template has been rendered/updated).
		//console.log("Template.topicRow.helpers.updateProgressBar("+topicId+")");
		var progress = computeProgress(topicId);
		var template = Template.instance();
		Meteor.defer(function() {
			//console.log("Template.topicRow.helpers.updateProgressBar....deferredFunc");
			//console.log(template);
			var progressBar = template.$('.progress');
			//console.log(progressBar);
			progressBar.progress({percent: progress});
			//progressBar.progress({label: "ratio", text: {ratio: "{value} of {total}"}});
		});
	},
});
