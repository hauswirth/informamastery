Template.topicFailedChecksPage.helpers({
	getAllStudentsTopicChecksForTopic: function(topicId) {
		return TopicChecks.find({topicId: topicId}, {sort: {endDate: -1}});
	},
});
