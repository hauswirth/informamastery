Template.topicPageOld.helpers({
	skills: function(topicId) {
		return Skills.find({topicId: topicId});
	},
	studyTasks: function(topicId) {
		return StudyTasks.find({topicId: topicId});
	},

	studyTaskCompleted: function(studyTaskId) {
		var studyTask = StudyTasks.findOne({_id: studyTaskId});
		if (studyTask && studyTask.detail) {
			var sectionIds = studyTask.detail.sectionIds;
			return SectionStatus.find({sectionId: {$in: sectionIds}, userId: Meteor.userId()}).count()
				== sectionIds.length;
		} else {
			return false;
		}
	},
	countStudyTaskSections: function(studyTaskId) {
		console.log("countStudyTaskSections("+studyTaskId+")");
		var studyTask = StudyTasks.findOne({_id: studyTaskId});
		if (studyTask && studyTask.detail) {
			return studyTask.detail.sectionIds.length;
		} else {
			return 0;
		}
	},
	countOwnStudyTaskSectionStatuses: function(studyTaskId) {
		console.log("countOwnStudyTaskSectionStatuses("+studyTaskId+")");
		var studyTask = StudyTasks.findOne({_id: studyTaskId});
		if (studyTask && studyTask.detail) {
			var sectionIds = studyTask.detail.sectionIds;
			return SectionStatus.find({sectionId: {$in: sectionIds}, userId: Meteor.userId()}).count();
		} else {
			return 0;
		}
	},

	getLabsForTopic: function(topicId) {
		return Labs.find({topicIds: topicId});
	},
	findOwnPracticeProblems: function(topicId) {
		return PracticeProblems.find({topicId: topicId, userId: Meteor.userId(), published:true, archived:false});
	},
	countOwnPracticeProblems: function(topicId) {
		return PracticeProblems.find({topicId: topicId, userId: Meteor.userId(), published:true, archived:false}).count();
	},
	countUnpublishedOwnPracticeProblems: function(topicId) {
		return PracticeProblems.find({topicId: topicId, userId: Meteor.userId(), published:false, archived:false}).count();
	},
	findOtherPracticeProblems: function(topicId) {
		return PracticeProblems.find({topicId: topicId, userId: {$ne: Meteor.userId()}, published:true, archived:false});
	},
	countOtherPracticeProblems: function(topicId) {
		return PracticeProblems.find({topicId: topicId, userId: {$ne: Meteor.userId()}, published:true, archived:false}).count();
	},
	findFeaturedPracticeProblems: function(topicId) {
		return PracticeProblems.find({topicId: topicId, published:true, archived:false, featured: true});
	},
	countFeaturedPracticeProblems: function(topicId) {
		return PracticeProblems.find({topicId: topicId, published:true, archived:false, featured: true}).count();
	},
	haveSolvedPracticeProblem: function(practiceProblemId) {
		return PracticeProblemSolutions.find({practiceProblemId: practiceProblemId, userId: Meteor.userId()}).count()>0;
	},
	isMe: function(userId) {
		return Meteor.userId()==userId;
	},
	isSkillReady: function(skillId) {
		var skillStatus = SkillStatus.findOne({studentId: Meteor.userId(), skillId: skillId});
		return skillStatus?skillStatus.ready:false;
	},
	isSkillRed: function(skillId) {
		var skillStatus = SkillStatus.findOne({studentId: Meteor.userId(), skillId: skillId});
		return skillStatus?skillStatus.status=="Red":false;
	},
  isSkillAmber: function(skillId) {
		var skillStatus = SkillStatus.findOne({studentId: Meteor.userId(), skillId: skillId});
		return skillStatus?skillStatus.status=="Amber":false;
	},
  isSkillGreen: function(skillId) {
		var skillStatus = SkillStatus.findOne({studentId: Meteor.userId(), skillId: skillId});
		// skillStatus.ready implies skillStatus.status=="Green"
		return skillStatus?skillStatus.ready:false;
	},
	hasOwnBookmark: function(skillId) {
		return Annotations.find({skillId: skillId, userId: Meteor.userId(), kind: "bookmark", removed: false}).count()>0;
	},
	getOwnAnnotations: function(skillId) {
		return Annotations.find({skillId: skillId, userId: Meteor.userId(), removed: false});
	},
	getOtherQuestions: function(skillId) {
		return Annotations.find({skillId: skillId, userId: {$ne: Meteor.userId()}, kind: "question", removed: false});
	},
	hasAnnotationsToDisplay: function(skillId) {
		var count =
			Annotations.find({skillId: skillId, userId: Meteor.userId(), removed: false}).count()+
			Annotations.find({skillId: skillId, userId: {$ne: Meteor.userId()}, kind: "question", removed: false}).count();
		console.log("Template.topicPageOld.helpers..hasAnnotationsToDisplay("+skillId+") -> "+count);
		return count;
	},
	isTopicToBeChecked: isTopicToBeChecked,
	isTopicMastered: isTopicMastered,
	isTopicReady: isTopicReady,
	canAddMoreTopicsToBeChecked: canAddMoreTopicsToBeChecked,
	getSuccessfulTopicCheckForTopic: function(topicId) {
		return TopicChecks.findOne({studentId: Meteor.userId(), topicId: topicId, mastered:true});
	},
	getTopicChecksForTopic: function(topicId) {
		return TopicChecks.find({studentId: Meteor.userId(), topicId: topicId});
	},
	getAllStudentsTopicChecksForTopic: function(topicId) {
		return TopicChecks.find({topicId: topicId});
	},
	//getTopicProgress: function(topicId) {
	//	console.log("Template.topicShow.helpers.getTopicProgress("+topicId+")");
	//	return computeProgress(topicId);
	//},
	updateProgressBar: function(topicId) {
		// Getting this progress bar to update reactively was really, REALLY painful!
		// Template.showTopic.rendered() is NOT called when the template changes reactively.
		// So to call some jQuery code ($('#progress').progress()) to get SemanticUI's progress bar
		// to update itself reactively has to be done otherwise.
		// After a LOT of StackOverflow and Google, I found out that we can use a helper (this one)
		// that will be triggered reactively (here, because the computeProgress() computation depends on changed values).
		// Then, given that the helper is run when the data changes (specifically, when the contents of the template is recomputed),
		// we need to somehow update the DOM. To access the DOM, we can get the Template.instance().
		// But at the time this helper runs, the DOM does not yet exist.
		// So we defer(), and only retrieve the progress bar's DOM node then (after the template has been rendered/updated).
		//console.log("Template.topicShow.helpers.updateProgressBar("+topicId+")");
		var progress = computeProgress(topicId);
		var context = this;
		var template = Template.instance();
		Meteor.defer(function() {
			//console.log("Template.topicShow.helpers.updateProgressBar....deferredFunc");
			//console.log(context);
			//console.log(template);
			var progressBar = template.$('#progress');
			//console.log(progressBar);
			progressBar.progress({percent: progress});
			//progressBar.progress({label: "ratio", text: {ratio: "{value} of {total}"}});
		});
	},
});


Template.topicPageOld.events({
	'click .add-topic-to-check-button': function(ev, template) {
		var topicId = this.topic._id;
		//console.log(".add-topic-to-check-button clicked. Topic's _id: "+topicId);
		Meteor.call('addTopicToBeChecked', topicId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .set-red-button': function(ev, template) {
		var skillId = this._id;
		console.log(".set-red-button clicked. Skill's _id: "+skillId);
		Meteor.call('setSkillStatusRed', skillId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .set-amber-button': function(ev, template) {
		var skillId = this._id;
		console.log(".set-amber-button clicked. Skill's _id: "+skillId);
		Meteor.call('setSkillStatusAmber', skillId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .set-green-button': function(ev, template) {
		var skillId = this._id;
		console.log(".set-green-button clicked. Skill's _id: "+skillId);
		Meteor.call('setSkillStatusGreen', skillId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .clear-button': function(ev, template) {
		var skillId = this._id;
		console.log(".clear-button clicked. Skill's _id: "+skillId);
		Meteor.call('setSkillStatusCleared', skillId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .create-practice-problem-button': function(event, template) {
		var topicId = this.topic._id;
		console.log("calling createPracticeProblem("+topicId+")");
		Meteor.call('createPracticeProblem', topicId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				console.log("success. result="+result);
				var practiceProblem = PracticeProblems.findOne({_id: result});
				Router.go("editPracticeProblemPage", {courseId: practiceProblem.courseId, topicId: practiceProblem.topicId, practiceProblemId: practiceProblem._id});
			}
		});
	},
});
