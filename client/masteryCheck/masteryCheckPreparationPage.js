Template.masteryCheckPreparationPage.rendered = function() {
  var self = this;
	console.log("Template.masteryCheckPreparationPage.rendered", self);
  this.$('.lab-dropdown').dropdown();
};

Template.masteryCheckPreparationPage.helpers({
  getLabsWithTeams: function(courseId) {
    var labIds = LabTeams.find({courseId: courseId, memberIds: Meteor.userId()}).map(function(labTeam) {return labTeam.labId});
    console.log("labIds: ", labIds)
    return Labs.find({courseId: courseId, _id: {$in: labIds}});
  },
  getLabTeam: function(labId) {
    return LabTeams.findOne({labId: labId, memberIds: Meteor.userId(), removed: {$ne: true}});
  },
  moreThanOneElements: function(array) {
    return array.length>1;
  },
  getMyMasteryCheckRegistration: function(courseId) {
    return MasteryCheckRegistrations.findOne({courseId: courseId, studentIds: Meteor.userId()});
  },
  getTopics: function(topicIds) {
    if (topicIds) {
      return Topics.find({topicId: {$in: topicIds}});
    } else {
      return null;
    }
  },
  isRegistrationEnqueued: function(masteryCheckRegistrationId) {
    return MasteryCheckQueueEntries.find({masteryCheckRegistrationId: masteryCheckRegistrationId}).count()>0;
  },
  doesRegistrationHaveCheck: function(masteryCheckRegistrationId) {
    return MasteryChecks.find({masteryCheckRegistrationId: masteryCheckRegistrationId}).count()>0;
  },
  getRegistrationCheck: function(masteryCheckRegistrationId) {
    return MasteryChecks.findOne({masteryCheckRegistrationId: masteryCheckRegistrationId});
  },
});

Template.masteryCheckPreparationPage.events({
  'submit .form': function(event, template) {
		event.preventDefault();
    var courseId = this._id;
    console.log("courseId", courseId);
    var labId = $(".lab-input").val();
    console.log("labId", labId);
    Meteor.call('createMasteryCheckRegistration', courseId, labId, function(error, result) {
      if (error) {
        throwError(error.reason);
      }
    });
  },
  'click .remove-topic-button': function(ev, template) {
    console.log("remove-topic-button clicked");
		var topicId = ""+this;
    console.log("topicId", topicId);
    var reg = MasteryCheckRegistrations.findOne({studentIds: Meteor.userId()});
    var masteryCheckRegistrationId = reg._id;
    console.log("masteryCheckRegistrationId", masteryCheckRegistrationId);
    Meteor.call('removeTopicFromMasteryCheckRegistration', masteryCheckRegistrationId, topicId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
  },
  'click .delete-mastery-check-registration-button': function(ev, template) {
    console.log("delete-mastery-check-registration-button clicked");
		var masteryCheckRegistrationId = this._id;
    console.log("masteryCheckRegistrationId", masteryCheckRegistrationId);
		Meteor.call('deleteMasteryCheckRegistration', masteryCheckRegistrationId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .enqueue-mastery-check-registration-button': function(ev, template) {
    console.log("enqueue-mastery-check-registration-button clicked");
		var masteryCheckRegistrationId = this._id;
    console.log("masteryCheckRegistrationId", masteryCheckRegistrationId);
		Meteor.call('enqueueMasteryCheckRegistration', masteryCheckRegistrationId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
  },
  'click .dequeue-mastery-check-registration-button': function(ev, template) {
    console.log("dequeue-mastery-check-registration-button clicked");
		var masteryCheckRegistrationId = this._id;
    console.log("masteryCheckRegistrationId", masteryCheckRegistrationId);
		Meteor.call('dequeueMasteryCheckRegistration', masteryCheckRegistrationId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
  },

});
