Template.header.events({
  'click .stopImpersonating': function() {
    Session.set("impersonating", false);
    location.reload();
  },
});
