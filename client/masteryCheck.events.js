Template.masteryCheckPage.events({
	'click .finish-mastery-check-button': function(ev, template) {
		console.log("finish-mastery-check-button clicked");
		var masteryCheck = Template.currentData();
		var courseId = masteryCheck.courseId;
		var studentId = masteryCheck.studentId;
		var studentRegistration = StudentRegistrations.findOne({courseId: courseId, studentId: studentId});
		Meteor.call('finishMasteryCheck', masteryCheck._id, function(error, masteryCheckId) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("studentPage", {courseId: courseId, _id: studentRegistration._id});
			}
		});
	},
	/*
	'click .set-mastered-button': function(ev, template) {
		var skillId = this._id;
		var topicId = this.topicId;
		var masteryCheckId = Template.currentData()._id;
		console.log(".set-mastered-button clicked: skillId: "+skillId+", topicId: "+topicId+", masteryCheckId: "+masteryCheckId);
		Meteor.call('setMasteryCheckSkillMastered', masteryCheckId, topicId, skillId, true, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .clear-mastered-button': function(ev, template) {
		var skillId = this._id;
		var topicId = this.topicId;
		var masteryCheckId = Template.currentData()._id;
		console.log(".set-mastered-button clicked: skillId: "+skillId+", topicId: "+topicId+", masteryCheckId: "+masteryCheckId);
		Meteor.call('setMasteryCheckSkillMastered', masteryCheckId, topicId, skillId, false, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'blur .topic-feedback': function(ev, template) {
		var topicId = this.topicId;
		var masteryCheckId = Template.currentData()._id;
		console.log(".topic-feedback area blurred: topicId: "+topicId+", masteryCheckId: "+masteryCheckId);
	    var feedback = ev.target.value;
		Meteor.call('setMasteryCheckTopicFeedback', masteryCheckId, topicId, feedback, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	*/
	'blur .mastery-check-notes': function(ev, template) {
		var masteryCheckId = Template.currentData()._id;
		console.log(".mastery-check-notes area blurred: masteryCheckId: "+masteryCheckId);
		var notes = ev.target.value;
		Meteor.call('setMasteryCheckNotes', masteryCheckId, notes, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .delete-mastery-check-button': function(e, t) {
		var masteryCheckId = Template.currentData()._id;
		console.log(".delete-mastery-check-button clicked: masteryCheckId: "+masteryCheckId);
		var masteryCheck = Template.currentData();
		Meteor.call('deleteMasteryCheck', masteryCheckId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				var courseId = masteryCheck.courseId;
				var studentId = masteryCheck.studentId;
				var studentRegistration = StudentRegistrations.findOne({courseId: courseId, studentId: studentId});
				Router.go("studentPage", {courseId: courseId, _id: studentRegistration._id});
			}
		});
	},
	'click .reopen-mastery-check-button': function(e, t) {
		var masteryCheckId = Template.currentData()._id;
		console.log(".reopen-mastery-check-button clicked: masteryCheckId: "+masteryCheckId);
		Meteor.call('reopenMasteryCheck', masteryCheckId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});

Template.topicCheckTablePart.events({
	'blur .topic-feedback': function(ev, template) {
		var topicCheckId = Template.currentData()._id;
		console.log(".topic-feedback area blurred: topicCheckId: "+topicCheckId);
	    var feedback = ev.target.value;
		Meteor.call('setTopicCheckFeedback', topicCheckId, feedback, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});

Template.skillCheckRow.events({
	'click .set-mastered-button': function(ev, template) {
		var skillCheckId = Template.currentData()._id;
		console.log(".set-mastered-button clicked: skillCheckId: "+skillCheckId);
		Meteor.call('setSkillCheckMastered', skillCheckId, true, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .clear-mastered-button': function(ev, template) {
		var skillCheckId = Template.currentData()._id;
		console.log(".clear-mastered-button clicked: skillCheckId: "+skillCheckId);
		Meteor.call('setSkillCheckMastered', skillCheckId, false, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
