// Client-side functions related to the "status" of users used by various helpers

getTopicsForCourseToBeChecked = function(courseId) {
	return getTopicsOfStudentForCourseToBeChecked(Meteor.userId(), courseId);
}

getTopicsOfStudentForCourseToBeChecked = function(studentId, courseId) {
	console.log("getTopicsOfStudentForCourseToBeChecked("+studentId+", "+courseId+")");
	// only show those for which topicStatus shows they are to be checked
	var tss = TopicStatus.find({courseId: courseId, studentId: studentId, toBeChecked: true});
	// extract topicIds from tss
	console.log(tss.fetch());
	var topicIds = tss.map(function(doc, index, cursor) {
		return doc.topicId;
	});
	console.log(topicIds);
	// find all topics with the given ids
	var ts = Topics.find({_id: {$in: topicIds}}, {sort: {masteryRequiredForGrade: 1}} );
	return ts;
}

computeProgress = function(topicId) {
    var readySkills = SkillStatus.find({studentId: Meteor.userId(), topicId: topicId, ready: true}).count();
	//console.log(" -- readySkills: "+readySkills);
    var totalSkills = Skills.find({topicId: topicId}).count();
	//console.log(" -- totalSkills: "+totalSkills);
	var progress = Math.round(100*readySkills/totalSkills);
	//console.log(" -- progress: "+progress);
    return ""+progress;
}

isTopicToBeChecked = function(topicId) {
	return TopicStatus.find({studentId: Meteor.userId(), topicId: topicId, toBeChecked: true}).count()>0;
}

isTopicMastered = function(topicId) {
	return TopicStatus.find({$or: [{studentIds: Meteor.userId()}, {studentId: Meteor.userId()}], topicId: topicId, mastered: true}).count()>0;
}

isTopicReady = function(topicId) {
	return computeProgress(topicId)==="100";
};

canAddMoreTopicsToBeChecked = function(topicId) {
	var courseId = Topics.findOne({_id: topicId}).courseId;
	var toBeCheckedCount = TopicStatus.find({studentId: Meteor.userId(), courseId: courseId, toBeChecked: true}).count();
	var maxTopicsPerMasteryCheck = Courses.findOne({_id: courseId}).maxTopicsPerMasteryCheck;
	return toBeCheckedCount<maxTopicsPerMasteryCheck;
};
