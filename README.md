# Informa - A Platform for Asynchronous Blended Mastery Learning

Informa is a web-based platform for asynchronous blended mastery learning courses.
It allows instructors to structure their courses into *topics*, grouped into *themes*.
Each topic is a clearly delineated testable unit, defining a set of *skills*.
Students have to demonstrate mastery in each topic by mastering all its skills.
This is done in regular in-person *mastery checks* (with an instructor or a TA).
Informa provides the information about all themes, topics, and skills,
and it keeps track of each students' progress.

To study a topic, students follow a **Learn**, **Practice**, **Reflect**, **Construct** approach.

To first **learn** about the concepts of a topic,
each topic provides a list of *study tasks*.
A study task is an activity to be conducted independently by the student,
with the goal of acquiring the skills of the corresponding topic.
There are different kinds of study tasks, such as textbook readings,
videos, web-based resources, or focused exercises.

To **practice** the concepts of a topic,
Informa allows students to solve various *practice problems*.
Students then **reflect** on the topic by creating their own practice problems.
This approach is similar to the [PeerWise](https://peerwise.cs.auckland.ac.nz) system.

To **construct** solutions to realistic problems, Informa supports *labs*.
Labs are small mini-projects that students work on,
and which serve to practice and integrate skills from various topics.

Informa allows students take notes or ask questions
about specific skills or lab items.

## Principles
Informa tries to encourage intrinsic motivation and discourage extrinsic motivation.
It takes inspiration from Deci and Ryan's
[Self-Determination Theory (SDT)](http://www.selfdeterminationtheory.org/theory/):
> Conditions supporting the individual’s experience of
> **autonomy**, **competence**, and **relatedness**
> are argued to foster the most volitional and high quality forms
> of motivation and engagement for activities,
> including enhanced performance, persistence, and creativity.
> In addition SDT proposes that the degree
> to which any of these three psychological needs is unsupported or thwarted
> within a social context
> will have a robust detrimental impact on wellness in that setting.

Based on SDT's "Goal Contents Theory",
we try to emphasize intrinsic goals such as
**community**, **close relationships**, and **personal growth**,
and we deemphasize extrinsic goals such as
**grades**, **appearance**, and **popularity/fame**.
One consequence of this is
that we don't provide rankings of students by any metric.

## Licensing
We have not yet decided on how exactly to license Informa.
There are several parts to it:

* The software: maybe to be licensed under BSD or MIT or Apache license
* The static contents of the site (text and images not related to specific courses):
  maybe to be licensed under CC BY SA (http://creativecommons.org/licenses/by-sa/4.0/)
* The static contents of a course (as entered by the corresponding teaching team):
  maybe ask the team to pick a CC license (CC BY, CC BY SA, CC ND, CC BY NC, CC BY NC SA, CC BY NC ND)
* The dynamic contents of a course (e.g., student submissions, discussions, ...):
  no public sharing, thus no license (or do we even need to license to the other students?)

## Implementation
Informa is an application built on [Meteor](https://www.meteor.com/).

### Data Model
Given that Informa is built on Meteor, it uses MongoDB. Below we describe the most important MongoDB collections of Informa, and the most important fields in each collection. Most documents in most collection have a `courseId` field, which is used to quickly determine to which course that document belongs (e.g., via a query like `Skills.find({courseId: xxx})`).

Some collections contain documents that do not contain any student-related information. Those "static" collections include `Courses`, `Themes`, `Topics`, `Skills`, `StudyTasks`, `Labs`, and `ContentItems`. The instructor of a course can create, update, and delete such documents. Currently the UI for these operations is not yet complete (e.g., some information in topics can be updated, and skills and study tasks can be created, updated, and deleted).

Informa uses the standard Meteor way to handle user accounts, that is, there is a `Meteor.users` collection with a document for each user account. Everybody who wants to log in needs a document in that collection. The signup process creates that document.

Three collections contain course registration information: `StudentRegistrations`, `AssistantRegistrations`, and `InstructorRegistrations`. While students can self-register for a course, assistants and instructors currently have to be registered by an administrator (the first user account, created when Informa is first started). A single user can have different kinds of registrations for different courses (e.g., be a Student in one course, and an Assistant in another course).

We now describe the key collections and their fields.

#### Courses
* title (long-form of course title, e.g. "Programming Fundamentals 2")
* abbreviation (abbreviation of course title, e.g. "PF2")
* instance (instance of a course, such as the year/semester/term in which it takes place, e.g., "2015")
* description (on-sentence short description of the course, used in sub-headers)
* overview (one or two paragraph overview of the course contents)
* organization (course overview, in markdown, shown on an entire page)
* logoUrl (URL of the image of the course logo, shown in the course list)
* welcomeVideoUrl (not yet used, maybe we will deprecate this)
* visible (should this course be shown in the course list?)
* openForRegistration (should we allow students to register for this course now?)
* startDate (exact date of the start of the course, used for computing current week of course)
* endDate (end date of the course)
* maxTopicsPerMasteryCheck (how many topics students can combine into a check, e.g., 4)

#### Themes
* courseId (_id of the course this theme is part of)
* title (very short title of this theme)
* description (short description, less than one sentence, of this theme)
* why (one-paragraph argument for why studying the topics in this theme is relevant)
* icon (name of SemanticUI icon used to represent this theme in the UI)
* color (name of Semantic UI color used to represent this theme in the UI)

#### Topics
* courseId (_id of the course this topic is part of)
* themeId (_id of the theme this topic is part of)
* title (very short title of this topic)
* description (short description, less than one sentence, of this topic)
* masteryRequiredForGrade (grade, currently 2...10, for which this topic has to be mastered)

#### Skills
* courseId (_id of the course this skill is part of)
* topicId (_id of the topic this skill is part of)
* title (very short title of this skill)
* description (short description, about one sentence, of this skill)

#### StudyTasks
* courseId (_id of the course this study task is part of)
* topicId (_id of the topic this study task is part of)
* kind (the kind of study task, e.g., "Textbook", "Web", ...)
* title (very short title of this study task)
* description (short description, about one sentence, of this study task)
* body (full text, in markdown, of this study task -- we may replace this by ContentItems in the future)

#### Labs
* courseId (_id of the course this lab is part of)
* title (very short title of this lab)
* description (very short description, less than one sentence, of this lab)
* topicIds (array of _id of the key topics the lab covers)

#### ContentItems
A content item represents a chunk of content (e.g. a block of markdown text, a question, a section header, an image, a video, ...). That content may be part of a lab (or, in the future, part of an exercise study task, or some other "container"). A content item has a position (index) within its container.

* courseId (_id of the course this content item is part of)
* containerId (_id of the container, e.g. the lab, this content item is part of)
* containerCollection (name of the collection, e.g., "Labs", containing the container of this content item)
* index (the position of this item, 0...N, inside its container)

#### Annotations
A user who registered for a course can annotate each skill and each content item. All such annotations are stored in this collection. If a user deletes an annotation, the annotation is not removed, but just marked as removed and thus not shown in the UI anymore.

* courseId (_id of the course this annotation is part of)
* skillId (optional: _id of the skill this annotation applies to)
* contentItemId (optional: _id of the content item this annotation applies to)
* userId (_id of the user who created this annotation)
* creationDate (date at which this annotation was created)
* modificationDate (date at which this annotation was last modified)
* removed (true, if this annotation was deleted; false, if it should be shown in the UI)
* text (the content, in markdown format, of the annotation)

#### TopicStatus
* courseId (_id of the course this topic status is part of)
* studentId (_id of the student this topic status is about)
* topicId (_id of the topic this topic status is about)
* toBeChecked (whether this topic is to be checked in the students' next mastery check)
* mastered (whether the student mastered this topic)
* masteredDate (date on which the student mastered this topic)

#### SkillStatus
* courseId (_id of the course this skill status is part of) - note: this field may not exist!
* studentId (_id of the student this skill status is about)
* topicId (_id of the topic this topic skill is about)
* skillId (_id of the skill this topic skill is about)
* ready (whether the student thinks they are ready to be checked on this skill)

#### ContentItemSubmissions
If a user registered for a course (as student, assistant, or instructor) submits their solution to a content item (e.g., by entering some text in a textarea that is part of a lab), Informa creates a content item submission document.

* courseId  (_id of the course this content item submission is part of)
* itemId (_id of the content item this submission is for)
* userId (_id of the user who created this submission)
* modificationDate (date this submission was last modified)
* value (the content of this submission; could be anything, depends on the kind of content item)

#### MasteryChecks
We use an interesting pattern to keep track of deleted mastery checks. We may use the same pattern for deleting all other kinds of documents in the future. We basically keep the document around (and thus its _id), but we replace its contents with three fields: `deleted` (which is set to `true`), `deletionDate`, `deletedBy`, and `original` (which is a subobject containing all the fields of the original document before it was deleted). This should allow us to create an UI to "undelete" a document without loss of information. This pattern requires the UI, or the publication, to filter out documents that contain a `deleted` field. E.g., via a query like `MasteryChecks.find({..., deleted: {$exists: false}})`.

* courseId (_id of the course this mastery check is part of)
* studentId (_id of the student registration of the student who is taking this mastery check)
* testerId (_id of the assistant or instructor registration who is administering this mastery check)
* startDate (time this check was started)
* endDate (time this check was finished)
* finished (true, if the check was finished; may be set to false again by reopening a check)
* reopened (true, if the instructor reopened this check, to fix some error)
* lastOverrideDate (date on which the instructor closed the check again, the last time it was reopened)
* notes (notes about this test for teaching team)
* notesDate (date on which the notes were updated)

Fields in deleted mastery checks:

* deleted (true if the instructor deleted this check; the UI should treat this check like it didn't exist)
* deletionDate (date on which the instructor deleted this check)
* deletedBy (userId of the user who deleted this check)
* original (copy of the JSON of the check that now has been deleted; a deleted mastery check document does **not** contain any of the above fields, except `deleted`, `deletionDate`, `deletedBy`, and `original`, and `original` is a subobject containing all the field of the original mastery check document)

#### TopicChecks
A topic check is part of a mastery check. A mastery check checks several topics. For each such topic we create a topic check.

* courseId (_id of the course this topic check is part of)
* studentId (_id of the student registration of the student who is taking this topic check)
* testerId (_id of the assistant or instructor registration who is administering this topic check)
* masteryCheckId (_id of the mastery check this topic check is part of)
* topicId (_id of the topic this check is checking)
* mastered (true if the student mastered this topic)
* feedback (feedback by the tester for the student about their performance on this test)
* feedbackDate (time the feedback was submitted)
* endDate (time this check was finished)

Fields in deleted topic checks:

* deleted
* deletionDate
* deletedBy
* original

#### SkillChecks
A skill check is part of a topic check. A topic check checks several skills. For each such skill we create a skill check.

* courseId (_id of the course this skill check is part of)
* studentId (_id of the student registration of the student who is taking this skill check)
* testerId (_id of the assistant or instructor registration who is administering this skill check)
* masteryCheckId (_id of the mastery check this skill check is part of)
* topicCheckId (_id of the topic check this skill check is part of)
* topicId (_id of the topic this check is checking)
* skillId (_id of the skill this check is checking)
* mastered (true if the student mastered this topic)
* date (time this check was done)

Fields in deleted skill checks:

* deleted
* deletionDate
* deletedBy
* original

#### PracticeProblems
A practice problem is created by a student. It is associated with a topic. It's essentially a multiple-choice question with an explanation.

* courseId
* topicId
* userId
* createdDate
* kind (currently only "MultipleChoice")
* title (short title to show in tables of problems)
* questionText (markdown)
* choices (array {label, text, isCorrect})
* explanation (markdown)
* published (the problem has been published and can be solved by others)
* publishedDate
* archived (the problem has been "removed" or replaced by another problem, and cannot be solved anymore)
* archivedDate
* clonedFromPracticeProblemId (if this was cloned from another practice problem)


#### PracticeProblemSolution
A solution to a practice problem, including rating information so the author of the question and the other students get to see what others think about the question.

* courseId
* topicId
* practiceProblemId
* userId (_id of user who submitted this solution)
* createdDate (date this solution was submitted)
* selectedChoices (array of indices into choices array, indicating the selected choices)
* difficulty (0=easy, 1=medium, 2=hard)
* quality (0=very poor, 1=poor, 2=fair, 3=good, 4=very good, 5=excellent)
* comment (feedback on the question)
* commentAgreementCount (number of other students who agreed with this comment)



### Publication/Subscription Model
We use a rather small number of publications. Thus, a client only needs to subscribe rather rarely (essentially whenever a user signs in and whenever the client UI switches to a different course).


#### courses
This includes the documents of the Courses collection.
A client wants to subscribe to this to show a list of all courses.

Collections:

* Courses


#### course-contents
This includes those documents defining the contents of a specific course.
A client subscribes to this to show details (e.g., themes, topics, skills, ...) about a specific course.

Subscription arguments:

* courseId

Collections:

* Courses
* Themes
* Topics
* Skills
* StudyTasks
* Labs
* ContentItems


#### my-user-contents
This includes those documents defining basic user information about the logged in user.

Subscription arguments: none.

Collections:

* Meteor.users
* StudentRegistrations
* AssistantRegistrations
* InstructorRegistrations

#### my-user-course-contents
This includes the contents relevant to a specific user in a specific course. Members of the teaching team of the course (and admins) receive documents related to all users of the course, while students receive documents related to themselves in this course.

Subscription arguments:

* courseId

Collections:

* StudentRegistrations
* AssistantRegistrations
* InstructorRegistrations
* Meteor.users
* Annotations
* ContentItemSubmissions
* TopicStatus
* SkillStatus
* MasteryChecks
* TopicChecks
* SkillChecks

## Deployment
Informa is still under construction, but it already is being used
in a Bachelor course at the University of Lugano.
The live instance of Informa for this course can be found at
[https://informa.inf.usi.ch](https://informa.inf.usi.ch).
